<?php session_start(); ?>

<!DOCTYPE HTML>
<html>



<head>

<link rel="stylesheet"  href="css/style.css" />
<link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="bootstrap/js/bootstrap.min.js"></script>
<meta charset="UTF-8" />
<style>
header
{
top: 0px;
}
</style>


</head>



<body>
<div id="wrapper">
<header>
<?php include("header2.php"); ?>
</header>


<section id="faq_section">


<h2><?php echo (($_SESSION['language']=='french')? "Conditions d'utilisation": "Terms of use"); ?></h2>


<p>
<?php if($_SESSION['language']=='french'){ ?>

Les présentes conditions d’utilisation constituent une entente légale entre vous et Teyyara.com relativement à l'accès et à l'utilisation du Site web. VEUILLEZ LIRE ATTENTIVEMENT LES CONDITIONS D'UTILISATION avant de naviguer sur le Site Web Teyyara.com

<?php }else{ ?>
The following terms of use, constitute a legal agreement between you and Teyyara.com related to any use of the Teyyara.com website. PLEASE READ CARFULLY THE TERMS OF USE before using Teyyara.com.
<?php } ?>

</p>
<div class="faq">
<p class="qustion">
<?php if($_SESSION['language']=='french'){ ?>
Acceptation des conditions d’utilisation :
<?php }else{ ?>
Accepting the terms of use :
<?php } ?>


</p>
<p class="answer">
<?php if($_SESSION['language']=='french'){ ?>
Lorsque vous naviguez sur le Site web Teyyara.com ou que vous utilisez une de ses fonctionnalités, incluant, notamment, la publication d’offres de transport, vous acceptez d'être lié par les Conditions d'Utilisation. SI VOUS NE SOUHAITEZ PAS ACCEPTER LES CONDITIONS D'UTILISATION, VEUILLEZ CESSER TOUTE UTILISATION DU SITE WEB. Les Conditions d'Utilisation peuvent être modifiées en tout temps par Teyyara.com à son entière discrétion. Votre utilisation du Site web et/ou de ses Fonctionnalités, sera considérée comme une acceptation des Conditions d'Utilisation. Nous vous prions de bien vouloir visiter la présente page à intervalles réguliers.

<?php }else{ ?>

When you navigate on Teyyara.com or use its functionalities, including transport offers publication, you accept to be bound by the Teyyara.com Terms of Use. IF YOU DON’T WISH TO ACCEPT THE TERMS OF USE, PLEASE STOP ALL USE OF Teyyara.COM. The terms of use might change at any time by Teyyara.com in its sole discretion. Your use of the website and/or its functionalities is considered as an acceptation of the terms of use. Please consider visiting the terms of use web page regularly.

<?php }?>

</p>
</div>

<div class="faq">
<p class="qustion">
<?php if($_SESSION['language']=='french'){ ?>
Conditions d’inscription :

<?php }else{ ?>
Registration terms :
<?php } ?>

</p>
<p class="answer">
<?php if($_SESSION['language']=='french'){ ?>
Nos services sont entièrement gratuits et réservés aux personnes majeures et juridiquement responsables. Nos services ne sont pas disponibles pour les mineurs de moins de 18 ans. Si vous ne remplissez pas ces conditions, vous ne devez pas utiliser notre site web.

<?php }else{ ?>
Our services are entirely free and reserved to adults and legaly responsible person. Our services are not intended for minors less than 18 years old. If you do not meet these conditions, please do not use Teyyara.com.
<?php  } ?>

</p>
</div>

<div class="faq">
<p class="qustion">
<?php if($_SESSION['language']=='french'){ ?>
Résiliation :

<?php }else{ ?>
Termination :
<?php } ?>
</p>
<p class="answer">
<?php if($_SESSION['language']=='french'){ ?>
Teyyara.com se réserve le droit de mettre fin à votre droit de navigation et d'utilisation du Site web ou d'une partie de celui-ci ou des Fonctionnalités ou de l'une de celles-ci, à tout moment, sans motif et sans préavis, et ce, à son entière discrétion.

<?php }else{ ?>
Teyyara.com reserves the right to terminate your right of navigating or using the Teyyara.com website or a part of it or some functionalities, at any moment , without any reason or warning, in its sole discretion.

<?php }  ?>
</p>
</div>

<div class="faq">
<p class="qustion">
<?php if($_SESSION['language']=='french'){ ?>
Rôle de Teyyara.com :

<?php }else{ ?>
Teyyara.com's role :
<?php } ?>

</p>
<p class="answer">
<?php if($_SESSION['language']=='french'){ ?>
Le rôle de Teyyara.com n'est pas celui d'une "société de transport". Au contraire, notre site web n'est qu'un support en ligne dont l'objet est de permettre à n'importe qui, n'importe où et n'importe quand, de rechercher ou de proposer de transporter un bien et qui est conforme à la loi.
Nous hébergeons le contenu des annonces mises en ligne par les utilisateurs. Nous n'intervenons pas dans la transaction entre envoyeurs et transporteurs. En conséquence, nous n'exerçons aucun contrôle sur la qualité, la sûreté des objets à transporter, la véracité ou l'exactitude dans les annonces mises en ligne, la capacité des transporteurs à transporter lesdits biens ni la capacité des envoyeurs à payer lesdits services. Nous ne pouvons assurer que le transporteur ou envoyeur conclura la transaction.
<?php }else{ ?>
The purpose of Teyyara.com is not of a « transport company », rather, we offer an online service, allowing any person, anywhere at anytime, to find a traveler that can transport for you, or allowing you to publish a travel so you can transport any good that comply with the law.
We only store the content of the announcement published by users. We do not intervene in the transaction between the sender and the transporter. Consequently, we cannot ensure the transport quality, the accuracy of the published offers, the capacity of the transporter to deliver the agreed upon goods or the sender to pay the agreed upon fees. We cannot ensure that the transporter or the sender to complete the transaction.


<?php  ?>
</p>
</div>

<div class="faq">
<p class="qustion">
<?php if($_SESSION['language']=='french'){ ?>
Authentification :

<?php }else{} ?>
Athentication :
<?php } ?>
</p>
<p class="answer">
<?php if($_SESSION['language']=='french'){ ?>
L'authentification des utilisateurs sur Internet étant difficile, nous ne pouvons pas confirmer l'identité de chaque membre.

<?php }else{ ?>
The authenticity of the users in the internet being difficult, we cannot confirm the identity of every member.

<?php } ?>
</p>
</div>

<div class="faq">
<p class="qustion">
<?php if($_SESSION['language']=='french'){ ?>
Avis de non responsabilité :

<?php }else{ ?>
Disclaimer :

<?php } ?>
</p>
<p class="answer">
<?php if($_SESSION['language']=='french'){ ?>
Puisque nous n'intervenons pas dans les transactions entre envoyeurs et transporteurs, vous nous dégagez de toute responsabilité en cas de litige entre plusieurs utilisateurs pour toute réclamation, et tout dommage présent ou futur, présumé ou non, constaté ou non, résultant de manière directe ou indirecte de ces réclamations.
Teyyara.com ne pourra en aucun cas être tenu responsable vis à vis d'un utilisateur en matière d'utilisation ou utilisation incorrecte des services de ce site.
<?php }else{ ?>
Since we do not intervene in the transaction itself between the sender and transporter, we do not hold any responsibility or whatsoever in the case of a dispute, or any complain and any damage in the present or future  presumed or not, recorded or not, resulting directly or indirectly from these claims.
Teyyara.com cannot be held responsible against any user in a matter or use of the Teyyara.com website.


<?php } ?>

</p>
</div>


<p class="qustion">
<?php if($_SESSION['language']=='french'){ ?>
Biens interdits :
<?php }else{ ?>
Prohibited goods :
<?php }?>

</p>

<p class="answer">
<?php if($_SESSION['language']=='french') { ?>
<ul>
<li>	Alcool
</li>
<li>
	Armes à feu et explosifs
</li>
<li>

	Armes et couteaux
</li>

<li>
	Biens culturels
</li>

<li>
	Biens soumis à embargo
</li>

<li>
	Biens volés ou recelés
</li>

<li>
	Billets de loterie
</li>

<li>
	Cartes de crédit
</li>

<li>
	Contrats et billets
</li>

<li>
	Documents officiels
</li>

<li>
	Drogues et objets associés
</li>

<li>
	Equipements de surveillance interdits
</li>

<li>
	Equipements médicaux
</li>

<li>
	Fausses monnaies et faux timbres
</li>

<li>
	Feux d'artifice
</li>

<li>
	Fouilles, Fossiles et Minéraux
</li>

<li>
	Marchandises interdites ou réglementées
</li>

<li>
	Objets à caractère pédophile ou pornographique
</li>

<li>
	Organes et produits du corps humain
</li>

<li>
	Parties animales
</li>

<li>
	Pesticides
</li>

<li>
	Substances dangereuses et illicites
</li>

<li>

	Tabac
</li>

<li>
	Tout produit interdit par la loi
</li>





</ul>

<?php }else{ ?>




<ul>
<li>Alcohol
</li>
<li>
Firearms and Explosives</li>
<li>
Weapons and Knives
</li>

<li>Cultural property
</li>

<li>Embargoed goods
</li>

<li>Stolen or harbored Goods
</li>

<li>Lottery Tickets
</li>

<li>Credit Cards
</li>

<li>Contracts and tickets
</li>

<li>Official documents
</li>

<li>Drugs and associated objects
</li>

<li>Banned Surveillance Equipment
</li>

<li>Medical equipment
</li>

<li>False false coins and stamps
</li>

<li>Fireworks
</li>

<li>Unreasonable Fossils and Minerals
</li>

<li>Restricted or prohibited goods
</li>

<li>Pedophile or pornographic objects
</li>

<li>Organs and products of the human body
</li>

<li>Animal Parts
</li>

<li>Pesticides
</li>

<li>Dangerous and Illegal Substances
</li>

<li>Tobacco

</li>

<li>Any product prohibited by law
</li>





</ul>
<?php } ?>
</p>
</section>

</div>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75334301-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
