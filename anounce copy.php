<div id="form">

 <?php
		if($_SESSION['language']==='english')
		{
		if(isset($_GET['dateError_announced']) && $_GET['dateError_announced']=='1') echo'<br><label class="field_error">Check your date to be in this format: day/month/year (ex: 29/01/2000) </label>';
		elseif(isset($_GET['dateMismatchError_announced']) && $_GET['dateMismatchError_announced']=='1') echo'<br><label class="field_error">The arrival date cannot be anterior to the departure date </label>';
		elseif(isset($_GET['timeError_announced']) && $_GET['timeError_announced']=='1') echo'<br><label class="field_error">Check your time to be in this format: hh:mm (ex: 21:30) </label>';
		elseif(isset($_GET['timeMismatchError_announced']) && $_GET['timeMismatchError_announced']=='1') echo'<br><label class="field_error">the arrival time cannot be before the departure time for the same day. </label>';
		elseif(isset($_GET['countryError_announced']) && $_GET['countryError_announced']=='1') echo'<br><label class="field_error">Check your departure or destination country info to be correct. </label>';
		elseif(isset($_GET['travelError_announced']) && $_GET['travelError_announced']=='1') echo'<br><label class="field_error">Check your departure or destination airport info to be correct. </label>';
    elseif(isset($_GET['typeError_announced']) && $_GET['typeError_announced']=='1') echo'<br><label class="field_error">You must select a type of what you are willing to deliver. </label>';
    elseif(isset($_GET['weightError_announced']) && $_GET['weightError_announced']=='1') echo'<br><label class="field_error">The weight must be a decimal number in kg. </label>';
		elseif(isset($_GET['priceError_announced']) && $_GET['priceError_announced']=='1') echo'<br><label class="field_error">The price must be a decimal number and you need to select a currency. </label>';
 //print_r($_SESSION['announce']);
		}
		else
		{
		if(isset($_GET['dateError_announced']) && $_GET['dateError_announced']=='1') echo'<br><label class="field_error">Vérifier que votre date est dans ce format: jour/mois/année (ex: 29/01/2000) </label>';
		elseif(isset($_GET['dateMismatchError_announced']) && $_GET['dateMismatchError_announced']=='1') echo'<br><label class="field_error">La date d\'arrivée ne peut pas être antérieure à la date de départ. </label>';
		elseif(isset($_GET['timeError_announced']) && $_GET['timeError_announced']=='1') echo'<br><label class="field_error">Vérifier que l\'heure est dans ce format hh:mm (ex: 21:30) </label>';
		elseif(isset($_GET['timeMismatchError_announced']) && $_GET['timeMismatchError_announced']=='1') echo'<br><label class="field_error">L\'heure d\'arrivée ne peut pas être avant l\'heure de départ. </label>';
		elseif(isset($_GET['countryError_announced']) && $_GET['countryError_announced']=='1') echo'<br><label class="field_error">Vérifier que votre pays de départ et d\'arrivé sont sélectionnés. </label>';
		elseif(isset($_GET['travelError_announced']) && $_GET['travelError_announced']=='1') echo'<br><label class="field_error">Vérifier que votre airoport de départ et d\'arrivé sont sélectionnés. </label>';
    elseif(isset($_GET['typeError_announced']) && $_GET['typeError_announced']=='1') echo'<br><label class="field_error">Vous devez selectionner le type de ce que vous voulez transporter. </label>';
    elseif(isset($_GET['weightError_announced']) && $_GET['weightError_announced']=='1') echo'<br><label class="field_error">Le poid doit être de valeur décimale en Kg.</label>';
		elseif(isset($_GET['priceError_announced']) && $_GET['priceError_announced']=='1') echo'<br><label class="field_error">Le prix doit être en valeur décimale et vous devez sélectionner la devise.</label>';

		}
 ?>


<form method="post" action="admin/add_anouncement.php"  >
<h2><?php if($_SESSION['language']==='french') echo'Formulaire d\'annonce';else echo'Announcement form';?></h2>
<fieldset class="col-lg-12">
   <legend><?php if($_SESSION['language']==='french') echo'Détail du voyage';else echo'Travel details';?></legend>

    <div class="col-lg-6" >

      <div class="col-lg-12">
       <label for="departure_date"><?php if($_SESSION['language']==='french') echo'Date de départ';else echo'Departure Date';?>:</label>
       <input type="text"  name="departure_date_announced"  id="departure_date_announced" class="datepicker" placeholder="dd/mm/yyyy" maxlength="10" <?php  if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true') echo 'value='.$_SESSION['announce']['departure_date'].'' ; ?> />
      </div>

      <div class="col-lg-12">
       <label for="departure_time"><?php if($_SESSION['language']==='french') echo'Heure de départ';else echo'Departure Time';?>:</label>
       <input type="text"  name="departure_time_announced"  id="departure_time_announced" class="timepicker" placeholder="hh:mm" maxlength="5"   <?php  if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true') echo 'value='.$_SESSION['announce']['departure_time'].'' ;?>  />
       </div>

      <div class="col-lg-12">
       <label for="id_from_country"><?php if($_SESSION['language']==='french') echo'Pays de départ ';else echo'Country of departure';?>:</label>
         <?php


      		$db_info=array('pdo'=>$db,'table'=>'countries');

      if($_SESSION['language']==='french')
       {

       $data_display=array('id_country','country');
       }
      else
       {

       $data_display=array('id_country','country_tr');
       }
      	if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true'  && isset($_SESSION['announce']['id_from_country']) )
      		$more=array('selected'=>array('type'=>'id','value'=>$_SESSION['announce']['id_from_country']),'class'=>'country_select','variable'=>'id_from_country_announced','data'=>'data-direction="id_from_announced" data-target="from_airport_target"','back_url'=>'index.php#anounce');
      	else
      		$more=array('variable'=>'id_from_country_announced','class'=>'country_select','data'=>'data-direction="id_from_announced" data-target="from_airport_target"','back_url'=>'index.php#anounce');
      		option_select($db_info,$data_display,$more);
      ?>
      </div>


      <div class="col-lg-12">
        <label for="id_from"><?php if($_SESSION['language']==='french') echo'Aéroport de départ ';else echo'Airport of departure';?>:</label>
        <?php
        if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true'  && isset($_SESSION['announce']['id_from_country']) && !empty($_SESSION['announce']['id_from_country']))
      	{
      	echo "<br><br><span id='from_airport_target' >";
      	$db_info=array('pdo'=>$db,'table'=>'airports');
        if($_SESSION['language']==='french')
        {
        $data_display=array('id_airport','airport');
        }
        else{

        $data_display=array('id_airport','airport_tr');

        }
         if(isset($_SESSION['announce']['id_from']))
      	$more=array('selected'=>array('type'=>'id','value'=>$_SESSION['announce']['id_from']),'variable'=>'id_from_announced','condition'=>'where id_country= '.$_SESSION['announce']['id_from_country'].'','back_url'=>'index.php#anounce');
      	else
      	$more=array('variable'=>'id_from_announced','condition'=>'where id_country= '.$_SESSION['announce']['id_from_country'].'','back_url'=>'index.php#anounce');

      	option_select($db_info,$data_display,$more);
      	echo "</span>";
      	}
      	else
      	echo"<span id='from_airport_target' ><select></select></span>";
      ?>
      </div>

    </div>

  	<div class="col-lg-6" >
      <div class="col-lg-12">

        <label for="arrival_date"><?php if($_SESSION['language']==='french') echo'Date d\'arrivée';else echo'Arrival Date';?>:</label>
    		<input type="text"  name="arrival_date_announced"  id="arrival_date_announced" class="datepicker" placeholder="dd/mm/yyyy" maxlength="10" <?php  if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true') echo 'value='.$_SESSION['announce']['arrival_date'].'' ;?> />
      </div>
      <div class="col-lg-12">
        <label for="arrival_time"><?php if($_SESSION['language']==='french') echo'Heure d\'arrivée';else echo'Arrival Time';?>:</label>
    		<input type="text"  name="arrival_time_announced"  id="arrival_time_announced" class="timepicker" placeholder="hh:mm" maxlength="5" <?php  if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true') echo 'value='.$_SESSION['announce']['arrival_time'].'' ;?> />
      </div>

      <div class="col-lg-12">
        <label for="id_to"><?php if($_SESSION['language']==='french') echo'Pays d\'arrivé';else echo'Country of arrival';?>:</label>
        <?php


        		$db_info=array('pdo'=>$db,'table'=>'countries');
        if($_SESSION['language']==='french')
        {

        $data_display=array('id_country','country');
        }else{

        $data_display=array('id_country','country_tr');
        }
        		if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true'  && isset($_SESSION['announce']['id_to_country']))
        		$more=array('selected'=>array('type'=>'id','value'=>$_SESSION['announce']['id_to_country']),'variable'=>'id_to_country_announced','class'=>'country_select','data'=>'data-direction="id_to_announced" data-target="to_airport_target"','back_url'=>'index.php#anounce');
        		else
        		$more=array('variable'=>'id_to_country_announced','class'=>'country_select','data'=>'data-direction="id_to_announced" data-target="to_airport_target"','back_url'=>'index.php#anounce');
        		option_select($db_info,$data_display,$more);
        ?>
      </div>

      <div class="col-lg-12">
        <label for="id_from"><?php if($_SESSION['language']==='french') echo'Aéroport d\'arrivé ';else echo'Airport of arrival';?>:</label>
        <?php




    		if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true'  && isset($_SESSION['announce']['id_to_country']) && !empty($_SESSION['announce']['id_to_country']) )
    		{
    		echo "<br><br><span id='to_airport_target' >";
    		$db_info=array('pdo'=>$db,'table'=>'airports');
        if($_SESSION['language']==='french')
        {
        $data_display=array('id_airport','airport');
        }
        else{

        $data_display=array('id_airport','airport_tr');

        }
    		$more=array('selected'=>array('type'=>'id','value'=>$_SESSION['announce']['id_to']),'variable'=>'id_to_announced','condition'=>'where id_country= '.$_SESSION['announce']['id_to_country'].'','back_url'=>'index.php#anounce');
    		option_select($db_info,$data_display,$more);
    		echo "</span>";
    		}
    		else
    		echo"<span id='to_airport_target' ><select></select></span>";



    	   ?>
      </div>


    </div>

</fieldset>

<fieldset class="col-lg-12">
  <legend><?php if($_SESSION['language']==='french') echo'Détails de livraison';else echo'Delivery details';?></legend>



    <div id="bdetail_fields" class="col-lg-6">
      <div class="col-lg-12">
        <label for="id_type">Type:</label>
        <?php
          $db_info=array('pdo'=>$db,'table'=>'type');

          $data_display=($_SESSION['language']==='french')? array('id_type','name_type'):array('id_type','name_tr_type');
          if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true')
          $more=array('selected'=>array('type'=>'id','value'=>$_SESSION['announce']['id_type']),'variable'=>'id_type','back_url'=>'index.php#anounce');
          else
          $more=array('variable'=>'id_type','back_url'=>'index.php#anounce');
          option_select($db_info,$data_display,$more);
        ?>
      </div>
      <div class="col-lg-12">
        <label><?php if($_SESSION['language']==='french') echo'Poids';else echo'Weight';?>:</label>
        <?php
          $db_info=array('pdo'=>$db,'table'=>'weight');

          $data_display=array('id_weight','weight');
          if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true')
          $more=array('selected'=>array('type'=>'id','value'=>$_SESSION['announce']['id_weight']),'variable'=>'id_weight','back_url'=>'index.php#anounce');
          else
          $more=array('variable'=>'id_weight','back_url'=>'index.php#anounce');
          option_select($db_info,$data_display,$more);
        ?>
      </div>
      <div class="col-lg-12">
        <label><?php if($_SESSION['language']==='french') echo'Prix';else echo'Price';?>:</label>
        <input  type="text" max-lentgh="30" name="price" <?php  if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true') echo 'value='.$_SESSION['announce']['price'].'' ;?> />
      </div>
      <div  class="col-lg-12">
        <label><?php if($_SESSION['language']==='french') echo'Devise';else echo'Currency';?>:</label>
        <?php
          $db_info=array('pdo'=>$db,'table'=>'currency');
          $data_display=array('id_currency','iso','name_currency');

          if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true')
          $more=array('selected'=>array('type'=>'value','value'=>$_SESSION['announce']['name_currency']),'variable'=>'id_currency','optionval'=>'value','back_url'=>'index.php#anounce');
          else
          $more=array('variable'=>'id_currency','optionval'=>'value','back_url'=>'index.php#anounce');
          option_select($db_info,$data_display,$more);
        ?>
      </div>
    </div>



  <div id="bright_details" class="col-lg-6">
    <div class="col-lg-12">
      <label id="comment" ><?php if($_SESSION['language']==='french') echo'Commentaire';else echo'Comment';?>:</label>
      <textarea name="comment" ><?php  if(isset($_SESSION['announce']) && $_SESSION['announce']['error']=='true') echo ''.$_SESSION['announce']['comment'].'' ;?> </textarea>
    </div>
  </div>

</fieldset>

<div id="buttons" class="col-lg-12 ">
<input type="submit" name="add" <?php if($_SESSION['language']==='french') echo'value="Ajouter"';else echo'value="Add"';?> />
<input type="reset" <?php if($_SESSION['language']==='french') echo'value="Réinitialiser"';else echo'value="Reset"';?> />
</div>


</form>


</div>
<!---
<div id="background">
</div>--->
