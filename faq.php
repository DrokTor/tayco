<?php session_start(); ?>

<!DOCTYPE HTML>
<html>



<head>

<link rel="stylesheet"  href="css/style.css" />
<link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="bootstrap/js/bootstrap.min.js"></script>

<meta charset="UTF-8" />


<style>
header
{
top:0px;}
</style>

</head>



<body>
<div id="wrapper">
<header>
<?php include("header2.php"); ?>
</header>


<section id="faq_section">


<h2><?php echo (($_SESSION['language']=='french')? "FAQ": "FAQ"); ?></h2>



<?php if($_SESSION['language']=='french' ){  ?>
<div class="faq">

<p class="qustion">

Combien ça coute?


</p>
<p class="answer">
L’inscription et le service Teyyara sont offerts gratuitement, les seuls frais à payer sont ceux du transporteur, le montant est indiqué dans le détail de l’annonce et c’est le transporteur qui le détermine.
</p>
</div>

<div class="faq">
<p class="qustion">
Avez-vous un numéro de téléphone pour vous joindre ?
</p>
<p class="answer">

Ce site est conçu de manière à vous faciliter la tâche et à utilisation autonome, donc pas besoin d’un intermédiaire vous pouvez vous-même ; soit mettre votre annonce pour transporter, soit trouver une annonce de transport qui vous convient. Ensuite, prenez contact avec la personne et définissez les modalités.
</p>
</div>

<div class="faq">
<p class="qustion">
Je ne comprends pas bien le fonctionnement entre Teyyara.com et le transporteur, est-ce que Teyyara.com collecte l’argent du transport et après elle paye le transporteur ?
</p>
<p class="answer">
Non, c’est la personne qui envoie un bien qui vous paye directement.

</p>
</div>

<div class="faq">
<p class="qustion">
Est-ce que c’est possible de consulter les annonces avant de m’inscrire ?
</p>
<p class="answer">
Oui, par le biais de la fonction "Trouver", vous pouvez consulter toutes les annonces mises en ligne sans s'inscrire.

</p>
</div>

<div class="faq">
<p class="qustion">
Comment trouver un transporteur  ?
</p>
<p class="answer">
 Vous pouvez trouver un transporteur dans le menu "Trouver", vous faites votre recherche, s'il y a un resultat, vous pouvez cliquer sur le boutton à droite pour voir les détail de l'offre
</p>
</div>

<div class="faq">
<p class="qustion">

Pour quelles zones géographiques ce service peut être utilisé ?
</p>
<p class="answer">
Ce service couvre les aéroports les plus importants du monde.
</p>
</div>
<?php }else{ ?>

<div class="faq">

<p class="qustion">

How much does it cost ?


</p>
<p class="answer">
The registration and service are free of any charge, if any, the only fees are the ones you pay to the transporter, the amount should be found in the detail of the offer. </p>
</div>

<div class="faq">
<p class="qustion">
How can I contact the transporter ?
</p>
<p class="answer">


The phone number of the sender can be found in the anouncement details, you can call him and speak with him.
</p>
</div>

<div class="faq">
<p class="qustion">

I don't understand how this website works, does Teyyara.com collect a fee and pay the transporter?
</p>
<p class="answer">
No, the sender pays the transporter directly and Teyyara.com doens't get any fee.
</p>
</div>

<div class="faq">
<p class="qustion">
Is it possible de search offers without registeration ?
</p>
<p class="answer">
Yes, you can use "Find" menu without registration.

</p>
</div>

<div class="faq">
<p class="qustion">
How to find a transporter ?
</p>
<p class="answer">
You can click on "Find" menu, enter the search options, if there is a result for your search, you can click on the right button for more details about the offer.
</p>
</div>

<div class="faq">
<p class="qustion">

Is this service available in all geographic zones ?
</p>
<p class="answer">
Yes, so far it covers the most important airports in the world.
</p>
</div>
<?php } ?>
</section>

</div>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75334301-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
