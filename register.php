<?php session_start(); ?>

<!DOCTYPE HTML>
<html>




<head>
  <!--
<link rel="stylesheet"  href="css/style.css" />
<link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui/jquery-ui.min.js"></script>--->
<link rel="stylesheet"  href="css/style.css" />
<link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui/jquery-ui.min.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<script src="bootstrap/js/bootstrap.min.js"></script>
<meta charset="UTF-8" />
</head>

<style>
header
{
  top:0px;
}
h2
{
text-align: center;
}
#register_section
{
/*width: 30%;*/
margin: 100px auto auto;
background-color: rgb(20, 118, 131);

color: white;
border-radius: 3px;
padding: 30px 60px;
box-shadow: -1px 1px 3px #383838;
}
#register_section input {
    width: 100%;
    line-height: 250%;
    border-radius: 1px;
    border: none;
    color: black !important;
}
body
{
background-color: #E9E7E7;


}
#submit
{
/*background-color: #002D30;*/
border: 1px solid #0DB4C9;
padding: 3px;
color: #0BBDF8 !important;
border-radius: 3px;
}
textarea
{
width: 99%;
margin-bottom: 10px;
color:black !important;
}
#agree-terms
{
margin-bottom: 40px;
}
#agree-terms input
{
  width: 30px;
}
#agree-terms a
{
color: #00dbff !important;
}


</style>


<body>
<div id="wrapper">
<header>
<?php include("header2.php"); ?>
</header>


<section id="register_section" class="col-xs-10 col-md-6 col-lg-4 no-float ">


<h2><?php echo (($_SESSION['language']=='french')? "Inscription": "Sign up"); ?></h2>
<form method="post" action="register_check.php"   >


<label for="first_name" ><?php echo (($_SESSION['language']=='french')? "Prénom": "First Name");
								if(isset($_GET['err']) && $_GET['err']==102 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? " ce champ doit être rempli."
											:" this field must be filled").'</span>' ;

								} ?></label><br>
<input type="text" name="first_name" id="first_name"  <?php if(isset($_SESSION['register']['first_name']) ) echo "value='".$_SESSION['register']['first_name']."'" ?>  /><br><br>
<label for="last_name" ><?php echo (($_SESSION['language']=='french')? "Nom": "Last Name");
								if(isset($_GET['err']) && $_GET['err']==103 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? " ce champ doit être rempli."
											:" this field must be filled").'</span>' ;

								}?></label><br>
<input type="text" name="last_name" id="last_name"   <?php if(isset($_SESSION['register']['last_name']) ) echo "value='".$_SESSION['register']['last_name']."'" ?> /><br><br>
<label for="password" title="<?php echo ( ($_SESSION['language']=='french')? "Le mot de passe doit contenir au minimum 6 caractères, une majuscule et un nombre ":"Password must contain at least 6 characters, Upper case letters and numbers" );?>" ><?php echo (($_SESSION['language']=='french')? "Mot de passe": "Password");
								if(isset($_GET['err']) && $_GET['err']==104 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? " ce champ doit être rempli."
											:" this field must be filled").'</span>' ;

								}elseif(isset($_GET['err']) && $_GET['err']==107 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? "Le mot de passe doit contenir 6 caractères au minimum, une majuscule et un nombre ":"Password must contain at least 6 characters, Upper case letters and numbers").'</span>' ;

								}?></label><span id="strengh"></span><br>
<input type="password" name="password"  id="password" <?php if(isset($_SESSION['register']['password']) ) echo "value='".$_SESSION['register']['password']."'" ?> data-lang="<?php echo ($_SESSION['language']==='french')? 'fr':'en';  ?>" title="<?php echo ( ($_SESSION['language']=='french')? "Le mot de passe doit comporter 6 caractères au minimum dont au moins une majuscule et un nombre ":"Password must contain more than 6 characters including Upper case letters and numbers" );?>"   /><br><br>
<label for="confirm_password" ><?php echo (($_SESSION['language']=='french')? "Mot de passe (confirmation)": "Password (confirmation)");
								if(isset($_GET['err']) && $_GET['err']==105 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? " ce champ doit être rempli."
											:" this field must be filled").'</span>' ;

								}elseif(isset($_GET['err']) && $_GET['err']==108 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? "Le mot de passe et la confirmation du mot de passe ne correspondent pas.":"The password and the password confirmation don't match.").'</span>' ;

								}?></label><span id="match" style="display;none;font-size: 1em;" ></span><br>
<input type="password" name="confirm_password"  id="confirm_password"  <?php if(isset($_SESSION['register']['confirm_password']) ) echo "value='".$_SESSION['register']['confirm_password']."'" ?>  /><br><br>
<label for="email" ><?php echo (($_SESSION['language']=='french')? "Email": "Email");
								if(isset($_GET['err']) && $_GET['err']==101 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? "Cette adresse email est déja enregistrée, merci de choisir une autre."
											:"This email address is already registred, please choose another one.").'</span>' ;

								}
								?></label><br>
<input type="email" name="email"  id="email"  <?php if(isset($_SESSION['register']['email']) ) echo "value='".$_SESSION['register']['email']."'" ?>   /><br><br>
<label for="phone" ><?php echo (($_SESSION['language']=='french')? "Tél": "Phone");
								if(isset($_GET['err']) && $_GET['err']==106 )
								{
									echo '<span style="color:#FFFC00;font-size: .9em;"> '.(($_SESSION['language']=='french')? " ce champ doit être rempli."
											:" this field must be filled").'</span>' ;

								}?></label><br>
<input type="tel" name="phone" id="phone"  <?php if(isset($_SESSION['register']['phone']) ) echo "value='".$_SESSION['register']['phone']."'" ?>  /><br><br>
<label for="address" ><?php echo (($_SESSION['language']=='french')? "Adresse": "Address"); ?></label><br>
<textarea name="address" id="address"  >
  <?php if(isset($_SESSION['register']['address']) ) echo $_SESSION['register']['address']  ?>
</textarea>
<div id="agree-terms">
  <input type="checkbox" name="agreed" value="true"/>
  <?php echo (($_SESSION['language']!='french')? '<span>I read and agree with the <a href="terms.php" target="_blank">terms</a> of use.</span>': '<span>J\'ai lu et accepte les <a href="terms.php" target="_blank">conditions</a> d\'utilisation.</span>');
  								if(isset($_GET['err']) && $_GET['err']==109 )
  								{
  									echo '<span style="color:#FFFC00;font-size: .9em;"><br> '.(($_SESSION['language']=='french')? " Vous devez accepter les conditions d'utilisation."
  											:" You need to accept the terms of use.").'</span>' ;

  								} ?>
</div>


<input  type="submit"   value="<?php echo (($_SESSION['language']=='french')? "S'inscrire": "Sign up"); ?>"  id="submit" />
</form>




</section>

</div>
</body>
<script>

$(document).ready(function(){

$('#password, #confirm_password').on('keyup', function () {
    if ($('#password').val() == $('#confirm_password').val()) {
        $('#match').html('  - ✓').css('color', '#0FF');
    } else
        $('#match').html('  - ✘').css('color', '#FFFC00');




	$('#strengh').html(checkStrength($('#password').val(),$('#password').data('lang')));



});

		function checkStrength(password,lang)
		{
		//initial strength
		var strength = 0

		//if the password length is less than 6, return message.
		if (password.length < 6) {
			$('#strengh').removeClass()
			$('#strengh').css('color','#FFBD7C')
//alert(lang)
			return  (lang==='fr')?'Trop court': '  Too short'
		}

		//length is ok, lets continue.

		//if length is 8 characters or more, increase strength value
		if (password.length > 7) strength += 1

		//if password contains both lower and uppercase characters, increase strength value
		if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1

		//if it has numbers and characters, increase strength value
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1

		//if it has one special character, increase strength value
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1

		//if it has two special characters, increase strength value
		if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1

		//now we have calculated strength value, we can return messages

		//if value is less than 2
		if (strength < 2 )
		{
			$('#strengh').removeClass()
			$('#strengh').css('color','#FCFF7C')
			return (lang==='fr')?'  Faible': '  Weak'
		}
		else if (strength == 2 )
		{
			$('#strengh').removeClass()
			$('#strengh').css('color','#C9FF7C')
			return (lang==='fr')?'  Bon':'  Good'
		}
		else
		{
			$('#strengh').removeClass()
			$('#strengh').css('color','#80F5FE')
			return (lang==='fr')?'  Puissant':'  Strong'
		}
		}

$('form').submit(function () {

    // Get the Login Name value and trim it
    var err,name = $.trim($('#first_name').val());
//alert($.session.get('language'));
    // Check if empty of not
    if (name  === '') {
		$('#first_name').css('border-right','10px solid  orange ');

       err=1;
    } else $('#first_name').css('border-right','none ');
	// Get the Login Name value and trim it
    var name = $.trim($('#last_name').val());
//alert($.session.get('language'));
    // Check if empty of not
    if (name  === '') {
		$('#last_name').css('border-right','10px solid  orange ');

       err=1;
    }  else $('#last_name').css('border-right','none ');
	// Get the Login Name value and trim it
    var name = $.trim($('#password').val());
//alert($.session.get('language'));
    // Check if empty of not
    if (name  === '') {
		$('#password').css('border-right','10px solid  orange ');

       err=1;
    }  else $('#password').css('border-right','none ');

	// Get the Login Name value and trim it
    var name = $.trim($('#confirm_password').val());
//alert($.session.get('language'));
    // Check if empty of not
    if (name  === '') {
		$('#confirm_password').css('border-right','10px solid  orange ');

       err=1;
    }  else $('#confirm_password').css('border-right','none ');

	// Get the Login Name value and trim it
    var name = $.trim($('#email').val());
//alert($.session.get('language'));
    // Check if empty of not
    if (name  === '') {
		$('#email').css('border-right','10px solid  orange ');

       err=1;
    }  else $('#email').css('border-right','none ');
	// Get the Login Name value and trim it
    var name = $.trim($('#phone').val());
//alert($.session.get('language'));
    // Check if empty of not
    if (name  === '' || !name.match(/^\d+$/)) {
		$('#phone').css('border-right','10px solid  orange ');

       err=1;
    } else $('#phone').css('border-right','none ');



	if(err === 1)
	{
		return false;
	}


});

});

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75334301-1', 'auto');
  ga('send', 'pageview');

</script>

</html>
