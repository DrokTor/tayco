<?php session_start();


   if(isset($_POST['french']))
   $_SESSION['language']='french' ;
   elseif(!isset($_SESSION['language']) || isset($_POST['english'])) $_SESSION['language']='english' ;
   if(!isset($_GET['id']) && !isset($_POST['resetpass'])) die();?>
   <!DOCTYPE HTML>
   <html>

  <head>
  <link rel="stylesheet"  href="css/style.css" />
  <link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-ui/jquery-ui.min.js"></script>
  <script src="js/jquery-ui-timepicker/jquery.ui.timepicker.js"></script>

  <meta charset="UTF-8" />
  </head>



  <body>

  <div id="main_wrapper" >
  <header>
  <?php //include("header.php");
  //include("top_menu.php"); ?>
  </header>
  <?php

  include("admin/database_connexion.php");

	if(isset($_POST['resetpass']) && isset($_POST['resetpass2']) && !empty($_POST['resetpass']) && !empty($_POST['resetpass2']) && $_POST['resetpass']===$_POST['resetpass2']
			&& (strlen($_POST['resetpass']) > 5 ) &&  preg_match('/[A-Z]+[a-z]+[0-9]+/', $_POST['resetpass']))
	{
		//echo"action";
		$email= $_SESSION['email_reset'];
		$pass= crypt($_POST['resetpass']);
		//echo $email.'  '.$pass;
		//print_r ($_SESSION);
		try{
		$q="UPDATE user SET password= ? WHERE email= ? ";
		$upd=$db->prepare($q);
		$upd->execute(array($pass,$email));
		//$prep->execute(array($_POST['submail']));

		if(!$upd){
		 $err='(code:'.$upd->errorInfo()[1].', message:'.$upd->errorInfo()[2].')';
		 throw new Exception($err);

		 }
		}
		catch(Exception $e)
		{

		 die('Error while getting information '.$e->getMessage().'<br><br><a href="index.php" class="option_button" >Ret    urn</a> ');


		}

		echo ($_SESSION['language']=='french')? "<div class='activation_msg'>Mot de passe réinitialisé avec succès.<br><a href='index.php#login' class='' >login</a></div>":
 "<div class='activation_msg'>Password successfully reset.<br><a href='index.php#login' class='' >login</a></div>";


		$qd="Delete From resetpass Where email= ?";
		$del=$db->prepare($qd);
		$del->execute(array($email));





	}
	elseif( isset($_POST['resetpass']) && isset($_POST['resetpass2']) && ($_POST['resetpass'] != $_POST['resetpass2']) )
	{
		echo ($_SESSION['language']=='french')? "<div class='activation_msg'> La confirmation est différente du mot de passe.</div> ":"<div class='activation_msg'> The confirmation is different from the password.</div> ";

	}
  elseif( isset($_POST['resetpass']) && isset($_POST['resetpass2']) && ((strlen($_POST['resetpass']) < 6 ) ||  !preg_match('/[A-Z]+[a-z]+[0-9]+/', $_POST['resetpass'])))
  {
    echo ($_SESSION['language']=='french')? "<div class='activation_msg'> le mot de passe doit être plus de 5 caractères et contient une majuscule et un chiffre.</div> ":
      "<div class='activation_msg'> The password must be more than 5 character and have at least one upper case latter and a number.</div> ";

  }
	elseif(isset($_GET['id']))
	{
	try{

  $q="SELECT email FROM resetpass WHERE hash= ? ";//AND password= ?$
  $prep=$db->prepare($q);
  $prep->execute(array($_GET['id']));//,$_POST['password']$
  //echo $q;$
  //$test_fetch=$test->fetch(PDO::FETCH_NUM);
	if(!$prep){
 $err='(code:'.$prep->errorInfo()[1].', message:'.$prep->errorInfo()[2].')';
 throw new Exception($err);

 }/*else echo"<div class='return_success'>information added successfully to the database.<br><br><a class='optio    n_button' href=".$back_url.">back</a></div> ";*/
 }
 catch(Exception $e)
 {

 die('Error while getting information '.$e->getMessage().'<br><br><a href="index.php" class="option_button" >Return</a> ');


 }
  $auth=$prep->fetch(PDO::FETCH_ASSOC);
  if(!$auth)
  {
  echo "Link expired.";
  }
  else{
	//print_r($auth);
	$_SESSION['email_reset']=$auth['email'];
  echo "<div id='passreset' class='activation_msg'>

 <form  method='post'  action='reset.php' >
 <label>Type your new password:</label><br><br>
 <input type='password' name='resetpass' /><br><br>
 <label>Confirm your new password:</label><br><br>
 <input type='password' name='resetpass2' /><br><br>

 <input type='submit' />

 </form>


 </div>
 ";

 }
}
   ?>




 </body>

 <footer>
 </footer>
 </div>
 <script>
</script>
</html>
