<?php

 session_start();

//echo  $_SERVER['HTTP_ACCEPT_LANGUAGE'];

 if(isset($_POST['logout']))  session_unset();
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
//echo $lang;
if($lang=='fr' && !isset($_SESSION['language'])) $_SESSION['language']='french' ;

if(isset($_POST['french']))
$_SESSION['language']='french' ;
elseif(!isset($_SESSION['language']) || isset($_POST['english'])) $_SESSION['language']='english' ;?>
<!DOCTYPE HTML>
<html>

<head>
<!-------><link rel="stylesheet"  href="css/style.css" />
<link rel="stylesheet" href="js/jquery-ui/jquery-ui.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">

<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui/jquery-ui.min.js"></script>
<script src="js/jquery-ui-timepicker/jquery.ui.timepicker.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<meta charset="UTF-8" />
<title>Teyyara- transport, deliver overseas</title>
</head>



<body>
  <?php
include("body_home.php");
?>
<div id="main_wrapper" class="container-fluid" >

<header class="row">
<?php include("header.php");
//include("top_menu.php"); ?>
</header>
<?php

include("admin/database_connexion.php");




//include("sep.php");
include("about.php");
//include("sep.php");
include("find.php");
//include("sep.php");
include("travel.php");
//include("sep.php");
//include("faq.php");
//include("sep.php");
include("contact.php");

  ?>



</body>

<footer>
</footer>
</div>
<script>
$(document).ready(function(){




    $(window).resize(function(){
      if($('#home_page').height() < 650 )
      {
      $('#bubble').css('margin-top', $('#home_page').height()* 0.05 );
      $('#bubble').css('margin-bottom', $('#home_page').height()* 0.05 );
      $('#bubble2').css('margin-bottom', $('#home_page').height()*0.1);
      }
      else
        {
          $('#bubble').css('margin-top', $('#home_page').height()* 0.12 );
          $('#bubble').css('margin-bottom', $('#home_page').height()* 0.12 );
          $('#bubble2').css('margin-bottom', $('#home_page').height()*0.18);

        }


    });
    $(window).resize();



		$(window).bind('scroll', function () {
			var pixels = $('#home_page').height() - $('header').height() ;//- $(window).height() * 0.12; //number of pixels before modifying styles
      //alert(pixels);
			if ($(window).scrollTop() > pixels) {
				$('header').addClass('fixed');
			} else {
				$('header').removeClass('fixed');
			}
		});
    $(window).trigger('scroll');


		$("#form").ready(function(){

		$(function() {
		$(".datepicker").datepicker({ dateFormat: 'dd/mm/yy' } );

		});

		$(function() {
		$(".timepicker").timepicker( {
			showPeriodLabels: false,
		} );
				});

		}) ;

    $('div[data-type="background"]').each(function(){
        var $bgobj = $(this); // assigning the object

        $(window).scroll(function() {
			var getInit=  $bgobj.css('backgroundPosition').split(" ");//initPos=0,alert(getInit[0]+' '+getInit[1]);
            var yPos = -($(window).scrollTop() / $bgobj.data('speed')) ;//+  (getInit[1].split("px"))[0] ;
            /*if( $bgobj.data('initial')>0 )
			{
			initPos=$bgobj.data('initial'); //getInit[1].slice(0,-2)//alert(yPos+'  '+getInit[1].slice(0,-2));
            yPos=yPos-100+initPos;
			//alert(initPos);
			$bgobj.attr('data-initial',yPos);
			}*/
			//if (yPos == 0) yPos=50;
			// Put together our final background position
			//alert(getInit[1]);
            var coords =  getInit[0]+' '+ yPos + 'px';


            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        });
    });


$(".country_select").change( function(event){

	//alert($("#cat_select option:selected").attr('value'));


	var value=$(":selected",event.target).attr('value'), var_value= $(event.target).data('direction'),
							targeted=$(event.target).data('target'), getlabel=$(event.target).data('label') ;
              //alert(var_value);
	var datasend= { get_airport : value , variable: var_value, label:getlabel };

	$.post("admin/ajax_select.php",datasend,function(data,status){

		//alert(targeted);
	//$( event.target ).next().html(data);

    $("#"+targeted ).html(data);
		}   );

} );





});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75334301-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
