<div >




<nav class="navbar " >


    <div class = "navbar-header  col-xs-2 col-sm-2  col-md-1 col-lg-2  ">
      <a href="index.php#home_page" class="navbar-brand"><img id="logo" src=" images/logo3.png"  alt="Teyyara logo" class="img-responsive" /></a>

    </div>



      <div  id="small-menu" class = "visible-xs visible-sm col-xs-5 col-sm-5  ">
      <ul id="menu_resp_sm" class=" nav nav-pills  collapse ">  <div class="row">
        <li class="col-md-2"><a href="index.php#home_page" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Accueil'; else echo 'Home';  ?></a></li>
        <li class="col-md-2"><a href="index.php#about_page" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'A propos'; else echo 'About';  ?> </a></li>
        <li class="col-md-2"><a href="index.php#find" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Trouver'; else echo 'Find';  ?> </a></li>
        <li class="col-md-2"><a href="index.php#anounce" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Publier'; else echo 'Post';  ?> </a></li>
        <li class="col-md-2"><a href="faq.php" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'FAQ'; else echo 'FAQ';  ?> </a>
        <li class="col-md-2"><a href="index.php#contact" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Contact'; else echo 'Contact';  ?> </a></li></div>
      </ul>
    </div>
      <div class="visible-xs visible-sm col-xs-3  col-sm-3 text-right">
      <button type = "button" class = "myCollapseButton"   data-toggle = "collapse" data-target = "#menu_resp_sm">
         <span class = "sr-only">Toggle navigation</span>
         <span class = "icon-bar"></span>
         <span class = "icon-bar"></span>
         <span class = "icon-bar"></span>
      </button>
      </div>

    <div  class = "hidden-sm  hidden-xs col-md-8 col-lg-8">
      <ul id="menu_resp" class=" nav nav-pills  collapse navbar-collapse">  <div class="row">
        <li class="col-md-2"><a href="index.php#home_page" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Accueil'; else echo 'Home';  ?></a></li>
        <li class="col-md-2"><a href="index.php#about_page" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'A propos'; else echo 'About';  ?> </a></li>
        <li class="col-md-2"><a href="index.php#find" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Trouver'; else echo 'Find';  ?> </a></li>
        <li class="col-md-2"><a href="index.php#anounce" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Publier'; else echo 'Post';  ?> </a></li>
        <li class="col-md-2"><a href="faq.php" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'FAQ'; else echo 'FAQ';  ?> </a>
        <li class="col-md-2"><a href="index.php#contact" class="round_menu" ><?php if($_SESSION['language']=='french') echo 'Contact'; else echo 'Contact';  ?> </a></li></div>
      </ul>
    </div>


<div class=" col-xs-5 col-sm-5 col-md-2 col-lg-1  text-right">
<div id="account" >

<?php
   if(isset($_SESSION['username']) )
 echo '<span id="username" >'.($_SESSION['username']).',</span>';

  if(!isset($_SESSION['username']) ) echo "<a href='index.php#anounce' class='logout'>".(($_SESSION['language']==='french')? 'se connecter': 'login')."</a>";
else echo '<form method="post" action="index.php"><input name="logout" class="logout" type="submit" value="'.(($_SESSION['language']==='french')?'Se déconnecter':'logout').'"/></form>';


// (($_SESSION['language']==='french')? 'bienvenue, ':'welcome, ').
?>
</div>
</div>

<div class=" col-xs-2 col-sm-2  col-md-1 col-lg-1  text-right">
<div id="language" >

<form   method="post"  action="index.php">
<input class="lang hidden-xs hidden-md" type="submit" value="Français" name="french" />
<input class="lang  visible-xs visible-md" type="submit" value="FR" name="french" />
</form>

<form    method="post"  action="index.php">
<input  class="lang hidden-xs hidden-md" type="submit" value="English" name="english" />
<input  class="lang visible-xs visible-md" type="submit" value="EN" name="english" />
</form>

</div>
  </div>

 </nav>
</div>
