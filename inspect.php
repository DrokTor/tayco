<?php
session_start();
if($_GET['inspect'])
{


include("admin/database_connexion.php");

$db_info=array("pdo"=>$db,"table"=>'travel',
"join"=>array("join1"=>array("table"=>"airports as air_from","on"=>"id_from=air_from.id_airport"),
"join2"=>array("table"=>"airports as air_to","on"=>"id_to=air_to.id_airport"),
"join3"=>array("table"=>"carriage","on"=>"travel.id_carriage=carriage.id_carriage"),
"join4"=>array("table"=>"user","on"=>"travel.id_user=user.id_user"),
"join5"=>array("table"=>"weight","on"=>"carriage.id_weight=weight.id_weight"),

"join6"=>array("table"=>"type","on"=>"carriage.id_type=type.id_type")
));

if($_SESSION['language']==='french')
{
$fields=array('departure_date','departure_time','air_from.airport as from_airport','arrival_date','arrival_time','air_to.airport as to_airport','name_type','weight','price','first_name','last_name','phone','address','email','comment');
}
else{
$fields=array('departure_date','departure_time','air_from.airport_tr as from_airport','arrival_date','arrival_time','air_to.airport_tr as to_airport','name_tr_type','weight','price','first_name','last_name','phone','address','email','comment');
}
$select_condition="id_travel=".$_GET['inspect']."";

$more=array("select_condition"=>$select_condition);


$raw_data=getRawData($db_info,$fields,$more);

$data=$raw_data->fetchAll(PDO::FETCH_ASSOC);
$data=$data[0];


echo '<div class="travel_info row ">
<div class="departure_info col-lg-6">';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Date de départ : ':'Departure date : ').'</label><label class="value_labels">'.$data['departure_date'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Heure de départ : ':'Departure time : ').'</label><label class="value_labels">'.$data['departure_time'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'De : ':'From : ').'</label><label class="value_labels">'.$data['from_airport'].'</label><br>';
echo '</div>';

echo '<div class="arrival_info  col-lg-6">';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Date d\'arrivée : ':'Arrival date : ').'</label><label class="value_labels">'.$data['arrival_date'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Heure d\'arrivée : ':'Arrival time : ').'</label><label class="value_labels">'.$data['arrival_time'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'À : ':'To : ').'</label><label class="value_labels">'.$data['to_airport'].'</label><br>';
echo '</div></div> ';

echo '<div class="travel_info row"><div class="delivery_info  col-lg-6">';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Type : ':'Type : ').'</label><label class="value_labels">'.(($_SESSION['language']==='french')? $data['name_type']:$data['name_tr_type']).'  </label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Poids : ':'Weight : ').'</label><label class="value_labels">'.$data['weight'].'  </label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Prix : ':'Price : ').'</label><label class="value_labels">'.$data['price'].' </label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Commentaire : ':'Comment : ').'</label><label class="value_labels">'.$data['comment'].'</label><br>';
echo '</div>';

echo '<div class="contact_info  col-lg-6">';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Prénom : ':'First name : ').'</label><label class="value_labels">'.$data['first_name'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Nom : ':'Last name : ').'</label><label class="value_labels">'.$data['last_name'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Tél : ':'Phone : ').'</label><label class="value_labels">'.$data['phone'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Email : ':'Email : ').'</label><label class="value_labels">'.$data['email'].'</label><br>';
echo '<label class="title_labels" >'.(($_SESSION['language']==='french')? 'Adresse : ':'Address : ').'</label><label class="value_labels">'.$data['address'].'</label><br>';
echo '</div></div>';



// print_r($data);
/*
while($data=$raw_data->fetch(PDO::FETCH_ASSOC))
{
foreach($data as $record )
{
echo $record."  ,  ";

}
echo "<br>";
}*/
}


?>
