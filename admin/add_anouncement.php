<?php session_start(); ?>

<?php

include("database_connexion.php");

if(isset($_POST['add']))
{

/// test date

if( validateDate($_POST['departure_date_announced'],"d/m/Y") && validateDate($_POST['arrival_date_announced'],"d/m/Y")
	&& ( ($_POST['departure_date_announced'] < $_POST['arrival_date_announced'])
	|| (( $_POST['departure_date_announced'] == $_POST['arrival_date_announced'] )  &&  $_POST['departure_time_announced'] < $_POST['arrival_time_announced']))

	&& validateDate($_POST['departure_time_announced'],"H:i") && validateDate($_POST['arrival_time_announced'],"H:i")
	&& !empty($_POST['id_from_country_announced']) && !empty($_POST['id_to_country_announced'])
	&& !empty($_POST['id_from_announced']) && !empty($_POST['id_to_announced']) && ($_POST['id_to_announced'] !=  $_POST['id_from_announced'])
	&& is_numeric($_POST['id_weight']) && !empty($_POST['id_weight']) &&  !empty($_POST['id_type'])
	&& !empty($_POST['price']) && is_numeric($_POST['price']) && !empty($_POST['id_currency'])
	)
{
/// test date

$data=array('id_type'=>$_POST['id_type'],'id_weight'=>$_POST['id_weight'],'price'=>$_POST['price'].' '.$_POST['id_currency'] ,'comment'=>$_POST['comment'], 'id_user'=>$_SESSION['id_user'] );
$pdo=array('db'=>$db,'table'=>'carriage');
$other=array('back_url'=>'../index.php#anounce');
$id_carriage=pdo_insert($pdo,$data,$other);

$data=array('id_from'=>$_POST['id_from_announced'],'id_to'=>$_POST['id_to_announced'],'departure_date'=>$_POST['departure_date_announced'],'departure_time'=>$_POST['departure_time_announced'],
			'arrival_date'=>$_POST['arrival_date_announced'],'arrival_time'=>$_POST['arrival_time_announced'],'id_carriage'=>$id_carriage, 'id_user'=>$_SESSION['id_user'], );
$pdo=array('db'=>$db,'table'=>'travel');
$other=array('back_url'=>'../index.php#anounce');
pdo_insert($pdo,$data,$other);

unset($_SESSION["announce"]);
header("location: ../index.php#anounce");
}else{
$_SESSION["announce"]["error"]="true";
$_SESSION["announce"]["departure_date"]=$_POST['departure_date_announced'];
$_SESSION["announce"]["arrival_date"]=$_POST['arrival_date_announced'];
$_SESSION["announce"]["departure_time"]=$_POST['departure_time_announced'];
$_SESSION["announce"]["arrival_time"]=$_POST['arrival_time_announced'];
$_SESSION["announce"]["id_from_country"]=$_POST['id_from_country_announced'];
$_SESSION["announce"]["id_from"]=$_POST['id_from_announced'];
$_SESSION["announce"]["id_to_country"]=$_POST['id_to_country_announced'];
$_SESSION["announce"]["id_to"]=$_POST['id_to_announced'];
$_SESSION["announce"]["id_type"]=$_POST['id_type'];
$_SESSION["announce"]["id_weight"]=$_POST['id_weight'];
$_SESSION["announce"]["price"]=$_POST['price'];
$_SESSION["announce"]["name_currency"]=$_POST['id_currency'];
$_SESSION["announce"]["comment"]=$_POST['comment'];




if(!validateDate($_POST['departure_date_announced'],"d/m/Y") || !validateDate($_POST['arrival_date_announced'],"d/m/Y"))
 header("location: ../index.php?dateError_announced=1#anounce");
elseif(( $_POST['departure_date_announced'] > $_POST['arrival_date_announced'] ))
 header("location: ../index.php?dateMismatchError_announced=1#anounce");
elseif(!validateDate($_POST['departure_time_announced'],"H:i") || !validateDate($_POST['arrival_time_announced'],"H:i"))
 header("location: ../index.php?timeError_announced=1#anounce");
elseif (( $_POST['departure_date_announced'] == $_POST['arrival_date_announced'] )  &&  $_POST['departure_time_announced'] >= $_POST['arrival_time_announced'])
 header("location: ../index.php?timeMismatchError_announced=1#anounce");
elseif(empty($_POST['id_from_country_announced']) || empty($_POST['id_to_country_announced']))
 header("location: ../index.php?countryError_announced=1#anounce");
elseif(empty($_POST['id_from_announced']) || empty($_POST['id_to_announced']) || ($_POST['id_to_announced'] ==  $_POST['id_from_announced']) )
 header("location: ../index.php?travelError_announced=1#anounce");
elseif(empty($_POST['id_type']))
 header("location: ../index.php?typeError_announced=1#anounce");
elseif(!is_numeric($_POST['id_weight']) || empty($_POST['id_weight']))
 header("location: ../index.php?weightError_announced=1#anounce");
elseif(empty($_POST['price']) || !is_numeric($_POST['price']) || empty($_POST['id_currency']) )
 header("location: ../index.php?priceError_announced=1#anounce");

 }

}





?>
