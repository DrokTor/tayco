-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 12, 2015 at 09:58 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tayara`
--
CREATE DATABASE IF NOT EXISTS `tayara` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tayara`;

-- --------------------------------------------------------

--
-- Table structure for table `airport`
--

CREATE TABLE IF NOT EXISTS `airport` (
  `id_airport` smallint(6) NOT NULL AUTO_INCREMENT,
  `name_airport` varchar(100) NOT NULL,
  PRIMARY KEY (`id_airport`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `airport`
--

INSERT INTO `airport` (`id_airport`, `name_airport`) VALUES
(1, 'houari boumedienne'),
(2, 'paris'),
(3, 'new york'),
(4, 'london'),
(5, 'sidney');

-- --------------------------------------------------------

--
-- Table structure for table `carriage`
--

CREATE TABLE IF NOT EXISTS `carriage` (
  `id_carriage` mediumint(9) NOT NULL AUTO_INCREMENT,
  `id_type` tinyint(4) NOT NULL,
  `weight` tinyint(10) NOT NULL,
  `price` varchar(50) NOT NULL,
  `id_currency` tinyint(4) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `id_user` mediumint(9) NOT NULL,
  PRIMARY KEY (`id_carriage`),
  KEY `id_info` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `carriage`
--

INSERT INTO `carriage` (`id_carriage`, `id_type`, `weight`, `price`, `id_currency`, `comment`, `id_user`) VALUES
(5, 0, 30, '127', 0, 'valise', 0),
(6, 0, 30, '127', 0, 'valise', 1),
(7, 1, 50, '127', 0, 'small thing', 1),
(8, 1, 50, '127', 0, 'small thing', 1),
(9, 1, 20, '127', 0, '/', 1),
(10, 1, 20, '127', 0, '/', 1),
(11, 1, 20, '127', 0, '/', 1),
(12, 3, 50, '127', 0, '', 1),
(13, 1, 20, '127', 0, 'fsdf', 1),
(14, 1, 20, '127', 0, 'fdsfsdf', 1),
(15, 1, 50, '127', 0, 'bonne affaire', 1),
(16, 1, 50, '127', 0, 'bonne affaire', 1),
(17, 1, 20, '127', 0, 'dsfsdf', 1),
(18, 2, 50, '127', 0, 'vcxvcx', 1),
(19, 1, 0, '0', 0, 'dfdf', 1),
(20, 1, 0, '0', 0, '', 1),
(21, 0, 50, '127', 0, ' dsdsds         ', 1),
(22, 0, 50, '127', 0, ' dsdsds         ', 1),
(23, 1, 1, '102', 0, '         only eur            ', 1),
(24, 1, 2, '200 EUR', 0, ' max 2 kg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE IF NOT EXISTS `currency` (
  `id_currency` smallint(6) NOT NULL AUTO_INCREMENT,
  `name_currency` varchar(10) NOT NULL,
  PRIMARY KEY (`id_currency`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id_currency`, `name_currency`) VALUES
(1, 'EUR'),
(2, 'USD'),
(3, 'DZD');

-- --------------------------------------------------------

--
-- Table structure for table `table 6`
--

CREATE TABLE IF NOT EXISTS `table 6` (
  `COL 1` varchar(10) DEFAULT NULL,
  `COL 2` varchar(10) DEFAULT NULL,
  `COL 3` varchar(11) DEFAULT NULL,
  `COL 4` varchar(53) DEFAULT NULL,
  `COL 5` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table 6`
--

INSERT INTO `table 6` (`COL 1`, `COL 2`, `COL 3`, `COL 4`, `COL 5`) VALUES
('', '', '', '', ''),
('', '', '', '', ''),
('', '', 'Pays', 'Aéroport', 'Type'),
('', '', 'Algérie', 'Alger-Houari Boumediene', 'I'),
('', '', 'Algérie', 'Béjaia-Soummam Abane Ramdane', 'I'),
('', '', 'Algérie', 'Ghardaïa-Moufdi Zakaria', 'I'),
('', '', 'Algérie', 'Hassi Messaoud-Oued Irara Krim Belkacem', 'I'),
('', '', 'Algérie', 'Chlef-Aboubakr Belkaid', ''),
('', '', 'Algérie', 'In Amenas-Zarzaitine', 'I'),
('', '', 'Algérie', 'Djanet-Tiska', ''),
('', '', 'Algérie', 'Tamanrasset-Aguenar Hadj Bey Akhamokh', 'I'),
('', '', 'Algérie', 'Oran-Ahmed Ben Bella', 'I'),
('', '', 'Algérie', 'Tlemcen-Zenata Messali El Hadj', 'I'),
('', '', 'Algérie', 'Annaba-Rabah Bitat', 'I'),
('', '', 'Algérie', 'Constantine-Mohamed Boudiaf', 'I'),
('', '', 'Algérie', 'Batna-Mostafa Ben Boulaid', 'I'),
('', '', 'Algérie', 'Jijel-Ferhat Abass', ''),
('', '', 'Algérie', 'Setif', ''),
('', '', 'Algérie', 'Biskra-Mohamed Khider', 'I'),
('', '', 'Algérie', 'Bou saâda', ''),
('', '', 'Algérie', 'Laghouat', ''),
('', '', 'Algérie', 'El Oued-Guemar', ''),
('', '', 'Algérie', 'Oum El Bouaghi', ''),
('', '', 'Algérie', 'Touggourt-Sidi Mahdi', ''),
('', '', 'Algérie', 'Hassi R''mel', ''),
('', '', 'Algérie', 'Ouargla', ''),
('', '', 'Algérie', 'Illizi', ''),
('', '', 'Algérie', 'In Salah', ''),
('', '', 'Algérie', 'El Golea', ''),
('', '', 'Algérie', 'In Guezzam', ''),
('', '', 'Algérie', 'Adrar-Touat Cheikh Sidi Mohamed Belkabir', 'I'),
('', '', 'Algérie', 'Béchar-Boudghene Ben Ali Lotfi', ''),
('', '', 'Algérie', 'El Bayadh', ''),
('', '', 'Algérie', 'Mascara-Ghriss', ''),
('', '', 'Algérie', 'Mecheria', ''),
('', '', 'Algérie', 'Tébessa-Cheikh Larbi Tébessi', 'I'),
('', '', 'Algérie', 'Tiaret-Abdelhafid Boussouf Bou Chekif', ''),
('', '', 'Algérie', 'Timimoun', ''),
('', '', 'Algérie', 'Tindouf', ''),
('', '', 'Tunisie', 'Djerba-Zarzis', ''),
('', '', 'Tunisie', 'El Borma', ''),
('', '', 'Tunisie', 'Enfidha-Hammamet', ''),
('', '', 'Tunisie', 'Gabès-Matmata', ''),
('', '', 'Tunisie', 'Gafsa-Ksar', ''),
('', '', 'Tunisie', 'Monastir Habib-Bourguiba', ''),
('', '', 'Tunisie', 'Sfax-Thyna', ''),
('', '', 'Tunisie', 'Tabarka-Aïn Draham', ''),
('', '', 'Tunisie', 'Tozeur-Nefta', ''),
('', '', 'Tunisie', 'Tunis-Carthage', ''),
('', '', 'Maroc', 'Casablanca-Mohammed V', ''),
('', '', 'Maroc', 'Marrakech -Menara', ''),
('', '', 'Maroc', 'Agadir-Al Massira', ''),
('', '', 'Maroc', 'Tanger-Ibn Batouta', ''),
('', '', 'Maroc', 'Fès-Saïss', ''),
('', '', 'Maroc', 'Oujda-Angad', ''),
('', '', 'Maroc', 'Nador-Al Aroui', ''),
('', '', 'Maroc', 'Rabat-Salé', ''),
('', '', 'Maroc', 'Essaouira', ''),
('', '', 'Maroc', 'Ouarzazate', ''),
('', '', 'France', 'Paris-Charles de Gaulle', ''),
('', '', 'France', 'Paris-Orly', ''),
('', '', 'France', 'Marseille-Provence', ''),
('', '', 'France', 'Nice-Côte d''Azur', ''),
('', '', 'France', 'Toulouse-Blagnac', ''),
('', '', 'France', 'Montpellier-Méditerranée', ''),
('', '', 'France', 'Lyon-Saint-Exupéry', ''),
('', '', 'France', 'Nantes-Atlantique', ''),
('', '', 'France', 'Lille-Lesquin', ''),
('', '', 'France', 'Strasbourg-Entzheim', ''),
('', '', 'France', 'Toulon/Hyères-Le Palyvestre', ''),
('', '', 'France', 'Bordeaux-Mérignac', ''),
('', '', 'France', 'Bastia-Poretta', ''),
('', '', 'France', 'Caen-Carpiquet', ''),
('', '', 'France', 'Ajaccio-Campo dell'' Oro', ''),
('', '', 'France', 'Rennes-Saint-Jacques', ''),
('', '', 'France', 'St Etienne-Bouthéon', ''),
('', '', 'Espagne', 'Alicante', ''),
('', '', 'Espagne', 'Barcelone', ''),
('', '', 'Espagne', 'Bilbao', ''),
('', '', 'Espagne', 'La Corogne', ''),
('', '', 'Espagne', 'Ibiza', ''),
('', '', 'Espagne', 'Lanzarote', ''),
('', '', 'Espagne', 'Madrid-Barajas', ''),
('', '', 'Espagne', 'Malaga', ''),
('', '', 'Espagne', 'Palma de Majorque', ''),
('', '', 'Espagne', 'Seville', ''),
('', '', 'Espagne', 'Tenerife', ''),
('', '', 'Espagne', 'Valence', ''),
('', '', 'Belgique', 'Bruxelles', ''),
('', '', 'Belgique', 'Liège', ''),
('', '', 'Belgique', 'Anvers', ''),
('', '', 'Pays-bas', 'Amsterdam-Schiphol', ''),
('', '', 'Pays-bas', 'Eindhoven', ''),
('', '', 'Pays-bas', 'Rotterdam', ''),
('', '', 'Italie', 'Alghero-Fertilia', ''),
('', '', 'Italie', 'Ancône-Falconara', ''),
('', '', 'Italie', 'Bari-Palese', ''),
('', '', 'Italie', 'Bologne-Guglielmo Marconi', ''),
('', '', 'Italie', 'Brindisi-Casale', ''),
('', '', 'Italie', 'Cagliari-Elmas', ''),
('', '', 'Italie', 'Catane-Fontanarossa', ''),
('', '', 'Italie', 'Florence-Peretola', ''),
('', '', 'Italie', 'Gênes-Cristoforo Colombo', ''),
('', '', 'Italie', 'L''Aquila', ''),
('', '', 'Italie', 'Lamezia-Terme', ''),
('', '', 'Italie', 'Lampedusa', ''),
('', '', 'Italie', 'Milan', ''),
('', '', 'Italie', 'Naples', ''),
('', '', 'Italie', 'Palerme-Punta Raisi', ''),
('', '', 'Italie', 'Pantelleria', ''),
('', '', 'Italie', 'Pescara-Abruzzo', ''),
('', '', 'Italie', 'Pise-Galileo Galilei', ''),
('', '', 'Italie', 'Reggio de Calabre', ''),
('', '', 'Italie', 'Rimini-Miramare', ''),
('', '', 'Italie', 'Rome-Fiumicino', ''),
('', '', 'Italie', 'Trapani-Birgi', ''),
('', '', 'Italie', 'Trieste-Ronchi dei Legionari', ''),
('', '', 'Italie', 'Turin-Caselle', ''),
('', '', 'Italie', 'Venise-Marco Polo', ''),
('', '', 'Italie', 'Verone-Villafranca', ''),
('', '', 'Allemagne', 'Berlin-Brandenburg', ''),
('', '', 'Allemagne', 'Berlin-Tegel', ''),
('', '', 'Allemagne', 'Brême', ''),
('', '', 'Allemagne', 'Cologne-Bonn', ''),
('', '', 'Allemagne', 'Dresde', ''),
('', '', 'Allemagne', 'Düsseldorf', ''),
('', '', 'Allemagne', 'Francfort', ''),
('', '', 'Allemagne', 'Hambourg', ''),
('', '', 'Allemagne', 'Hanovre', ''),
('', '', 'Allemagne', 'Munich', ''),
('', '', 'Allemagne', 'Munster-Osnabruck', ''),
('', '', 'Allemagne', 'Nuremberg', ''),
('', '', 'Allemagne', 'Paderborn', ''),
('', '', 'Allemagne', 'Stuttgart', ''),
('', '', 'Allemagne', 'Sarrebruck', ''),
('', '', 'Allemagne', 'Leipzig-Halle', ''),
('', '', 'Allemagne', 'Erfurt-Weimar', ''),
('', '', 'Royaume Uni', 'Aberdeen-Dyce', ''),
('', '', 'Royaume Uni', 'Belfast-George Best', ''),
('', '', 'Royaume Uni', 'Belfast-International Airport', ''),
('', '', 'Royaume Uni', 'Birmingham', ''),
('', '', 'Royaume Uni', 'Bristol', ''),
('', '', 'Royaume Uni', 'Cambridge', ''),
('', '', 'Royaume Uni', 'Cardiff ', ''),
('', '', 'Royaume Uni', 'Dundee', ''),
('', '', 'Royaume Uni', 'Durham-Tees Valley', ''),
('', '', 'Royaume Uni', 'Edimbourg', ''),
('', '', 'Royaume Uni', 'Exeter', ''),
('', '', 'Royaume Uni', 'Glasgow', ''),
('', '', 'Royaume Uni', 'Guernesey', ''),
('', '', 'Royaume Uni', 'Humberside ', ''),
('', '', 'Royaume Uni', 'Île de Man-Ronaldsway', ''),
('', '', 'Royaume Uni', 'Inverness', ''),
('', '', 'Royaume Uni', 'Jersey', ''),
('', '', 'Royaume Uni', 'Leeds-Bradford', ''),
('', '', 'Royaume Uni', 'Liverpool', ''),
('', '', 'Royaume Uni', 'Londres-Gatwick', ''),
('', '', 'Royaume Uni', 'Londres-Heathrow', ''),
('', '', 'Royaume Uni', 'Londres-Luton', ''),
('', '', 'Royaume Uni', 'Manchester', ''),
('', '', 'Royaume Uni', 'Newcastle', ''),
('', '', 'Royaume Uni', 'Norwich', ''),
('', '', 'Royaume Uni', 'Nottingham', ''),
('', '', 'Royaume Uni', 'Southampton', ''),
('', '', 'Canada', 'Calgary', ''),
('', '', 'Canada', 'Edmonton', ''),
('', '', 'Canada', 'Halifax', ''),
('', '', 'Canada', 'Moncton', ''),
('', '', 'Canada', 'Montréal-Pierre Elliott Trudeau', ''),
('', '', 'Canada', 'Ottawa', ''),
('', '', 'Canada', 'Québec', ''),
('', '', 'Canada', 'Regina', ''),
('', '', 'Canada', 'Saskatoon', ''),
('', '', 'Canada', 'St Johns', ''),
('', '', 'Canada', 'Toronto-City Centre Airport', ''),
('', '', 'Canada', 'Toronto-Pearson International', ''),
('', '', 'Canada', 'Vancouver', ''),
('', '', 'Canada', 'Winnipeg', ''),
('', '', 'Egypte', 'Le Caire', ''),
('', '', 'Egypte', 'Abou Simbel', ''),
('', '', 'Egypte', 'Marsa Alam', ''),
('', '', 'Egypte', 'El-Arish', ''),
('', '', 'Egypte', 'Charm el-Cheikh', ''),
('', '', 'Egypte', 'Hurghada', ''),
('', '', 'Egypte', 'Taba', ''),
('', '', 'Chine', 'Aéroport international de Pékin', ''),
('', '', 'Chine', 'Aéroport à Hong Kong', ''),
('', '', 'Chine', 'Aéroport international de Shanghai-Pudong', ''),
('', '', 'Chine', 'Aéroport international de Shanghai Hongqiao', ''),
('', '', 'Chine', 'Aéroport international de Canton Baiyun', ''),
('', '', 'Chine', 'Aéroport international de Shenzhen Bao''an', ''),
('', '', 'Chine', 'Aéroport international de Zhengzhou Xinzheng', ''),
('', '', 'Chine', 'Aéroport international de Kunming Changshui', ''),
('', '', 'Etats-Unis', 'Aéroport international John-F.-Kennedy, NY', ''),
('', '', 'Etats-Unis', 'Aéroport international Newark Liberty, NY', ''),
('', '', 'Etats-Unis', 'Aéroport international de Los Angeles, CA', ''),
('', '', 'Etats-Unis', 'Aéroport international de San Diego, CA', ''),
('', '', 'Etats-Unis', 'Aéroport international de San Francisco, CA', ''),
('', '', 'Etats-Unis', 'Aéroport international de Dallas-Fort Worth, TX', ''),
('', '', 'Etats-Unis', 'Aéroport intercontinental George-Bush de Houston, TX', ''),
('', '', 'Etats-Unis', 'Aéroport international de San Antonio, TX', ''),
('', '', 'Etats-Unis', 'Aéroport international de Miami, FL', ''),
('', '', 'Etats-Unis', 'Aéroport international d''Orlando, FL', ''),
('', '', 'Etats-Unis', 'Aéroport international O''Hare de Chicago, IL', ''),
('', '', 'Etats-Unis', 'Aéroport international Midway de Chicago, IL', ''),
('', '', 'Etats-Unis', 'Aéroport international de Gary/Chicago, IL', ''),
('', '', 'Etats-Unis', 'Aéroport international de Washington-Dulles', '');

-- --------------------------------------------------------

--
-- Table structure for table `travel`
--

CREATE TABLE IF NOT EXISTS `travel` (
  `id_travel` mediumint(9) NOT NULL AUTO_INCREMENT,
  `id_from` smallint(6) NOT NULL,
  `departure_date` varchar(10) NOT NULL,
  `departure_time` varchar(50) NOT NULL,
  `id_to` smallint(6) NOT NULL,
  `arrival_date` varchar(50) NOT NULL,
  `arrival_time` varchar(50) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `id_carriage` mediumint(9) NOT NULL,
  `id_user` mediumint(6) NOT NULL,
  PRIMARY KEY (`id_travel`),
  KEY `from` (`id_from`,`id_to`,`id_carriage`),
  KEY `to` (`id_to`),
  KEY `id_carriage` (`id_carriage`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `travel`
--

INSERT INTO `travel` (`id_travel`, `id_from`, `departure_date`, `departure_time`, `id_to`, `arrival_date`, `arrival_time`, `type`, `id_carriage`, `id_user`) VALUES
(1, 1, '10/12/2014', '11;30', 3, '10/12/2014', '17:20', 0, 11, 1),
(2, 1, '10/12/2014', '11;30', 2, '10/13/2014', '00:20', 0, 12, 1),
(3, 2, '10/13/2014', '11;30', 1, '10/13/2014', '17:20', 0, 13, 1),
(4, 5, '10/13/2014', '11;30', 4, '10/13/2014', '17:20', 0, 14, 1),
(5, 2, '17/12/2014', '10:20', 3, '18/12/2014', '00:00', 0, 15, 1),
(6, 2, '10/01/2015', '11;30', 1, '11/01/2015', '00:00', 0, 16, 1),
(7, 1, '10/02/2015', '08:00', 2, '10/02/2015', '12:00', 0, 17, 1),
(8, 3, '10/02/2015', '10:20', 2, '10/02/2015', '17:20', 0, 18, 1),
(9, 5, '02/04/2015', 'fdf', 3, '02/26/2015', 'dfdf', 0, 19, 1),
(10, 1, '1212121321', '02:05', 3, '02/26/2015', '01:15', 0, 20, 1),
(11, 1, '10/03/2015', '16:20', 2, '11/03/2015', '16:20', 0, 21, 1),
(12, 1, '10/03/2015', '16:20', 2, '11/03/2015', '16:20', 0, 22, 1),
(13, 1, '11/03/2015', '02:00', 3, '12/03/2015', '10:15', 0, 23, 1),
(14, 1, '11/03/2015', '16:10', 4, '12/03/2015', '20:40', 0, 24, 1);

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `id_type` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id_type`, `name_type`) VALUES
(1, 'small'),
(2, 'medium'),
(3, 'big');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id_user` mediumint(9) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(11) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `first_name`, `last_name`, `address`, `phone`, `email`, `password`) VALUES
(1, 'kouki', 'kouki', 'address', '1024500', '1@1.com', '1'),
(2, 'ath', '', '', '545046040', 'my@mail.com', 'ath'),
(3, 'ath', '', '', '545046040', 'my@mail.com', 'ath'),
(4, 'ath', '', '', '545046040', 'my@mail.com', 'ath');

-- --------------------------------------------------------

--
-- Table structure for table `users_temp`
--

CREATE TABLE IF NOT EXISTS `users_temp` (
  `id_temp` smallint(6) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(200) NOT NULL,
  `rand_temp` int(11) NOT NULL,
  PRIMARY KEY (`id_temp`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users_temp`
--

INSERT INTO `users_temp` (`id_temp`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `rand_temp`) VALUES
(1, 'ath', '', 'my@mail.com', 'ath', '545046040', '', 54),
(2, 'ath2', '', 'mymail@my.com', 'ath', '545046040', '', 54),
(3, 'ath3', '', 'mymail@mail.com', 'ath', '545046040', '', 54),
(4, 'ath3', '', 'mymail@mail.com', 'ath', '545046040', '', 54);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
