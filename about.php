<div  id="about_page" class=" row"  >



<div class="head" id="about_head">
</div>
<section id="about_content" class="col-xs-8 col-md-7 center-block no-float">

  <article class="row">
  <div id="about_cloud" class="col-md-12">

  <section class="row">
    <img src="images/new.png"class="img-responsive col-xs-8 col-sm-3 col-md-3 col-lg-2" />
    <article class="col-xs-12 col-sm-9 col-md-9">
    <h2 class=""><?php if($_SESSION['language']=='french') echo 'Un nouveau mode de livraison !'; else echo 'A new way of delivery !';  ?> </h2>
    <p ><?php if($_SESSION['language']=='french') echo 'Teyyara est une plateforme gratuite qui s’adresse à toute personne qui veut envoyer ou transporter un bien lors de ses déplacements personnels ou professionnels.

    Notre mission est de faciliter le transport de bien de tout genre, en offrant à nos clients un outil efficace et simple d’utilisation.';
     else echo 'Teyyara is a brand new way of delivery, it is a free platform that anyone can use should he wants to deliver or send something abroad via the Teyyara community. ';  ?>

    </p>
    </article>
  </section>

  <section id="about_detail" class="row">
    <div class="col-xs-12 col-md-6">
    <img src="images/search.png" class="img-responsive col-xs-6 col-sm-3  col-md-3 " />
     <article class="col-xs-12 col-sm-9 col-md-9">
      <h3><?php if($_SESSION['language']=='french') echo 'Trouver un transporteur'; else echo 'Find a transporter';  ?></h3>
      <p><?php if($_SESSION['language']=='french') echo 'On vous simplifie la tâche pour trouver une personne qui voyage de la localité et vers la destination qui vous intéresse.'; else echo 'We make it easy for you to find a person traveling from the location to the destination you\'re looking for.';  ?>
      </p>
      </article>
    </div>





      <div id="about_rightdiv" class="col-xs-12 col-md-6">
      <img src="images/infoman.png" class="img-responsive col-xs-6 col-sm-3 col-md-3" />
      <article class="col-xs-12 col-sm-9 col-md-9">
      <h3><?php if($_SESSION['language']=='french') echo 'Transporter'; else echo 'Transport';  ?> </h3>
      <p><?php if($_SESSION['language']=='french') echo 'Vous pouvez adhérer à la communauté du transport social en aidant d\'autres personnes dans leur besoin de livraison lors de vos voyages.'; else echo 'You can be part of the social delivery system community and help other people in their delivery needs during your trips.';  ?> </p>
      </article>
      </div>

  </section>
  </div>
  </article>

</section>
<div class="visible-xl fill-height">
</div>
</div>
