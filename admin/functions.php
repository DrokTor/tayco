<?php

////    return last inserted  ID added in pdo_insert 

///      insert pdo, msg removed

//// added compatibility with double join with the same table, you cna now use "table.field as field"  in parameters


//// added the possibility t odisplay ID or not in list data function

/// select function can set selected given the value or id of selected


/// added condition to option_select function

function pdo_connect($host,$dbname,$user,$pass,$more=null)
			{ 
				try
			   {
			      $db= New PDO('mysql:host='.$host.'; dbname='.$dbname.'',''.$user.'',''.$pass.'',[ PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'" ]);
					if(!$db)
					{
						$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
						throw new Exception($err);
					
					}
			   }
			   catch (Exception $e)
			   {
					die("Error while connecting to database ".$e->getMessage() .'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a> ');
			   
			   
			   }
				return $db;
			}

//--------------------------------------

//                    INSERT INTO 

//--------------------------------------

function pdo_insert($pdo, $data, $other =null)
{

$table=$pdo['table'];
$db=$pdo['db'];
$back_url=(isset($other['back_url']))? $other['back_url']:'';

$keys=array_keys($data);
$fields=array();
$values=array();
$tokens=array();
for($i=0,$c=count($keys) ; $i<$c ; ++$i)
{
if(isset($data[$keys[$i]]))
{
array_push($fields, $keys[$i] );
array_push($values, $data[$keys[$i]] );
array_push($tokens, '?' );
}



}

$fields=implode(',',$fields);
$tokens=implode(',',$tokens);
/*echo $fields.'<br>';
echo $tokens.'<br>';
print_r($values);*/




				try
				{	


$db_store= $db->prepare("INSERT INTO $table ($fields) VALUES ($tokens) ");
$result=$db_store->execute($values);



					if(!$result)
					{
						$err='(code:'.$db_store->errorInfo()[1].', message:'.$db_store->errorInfo()[2].')';
						throw new Exception($err);
					
					}else $err=0;
									
				}
				catch(Exception $e)
				{
					die('Error while inserting data '.$e->getMessage().'<br><br><a class="option_button" >Return</a> ');	
				}
return($db->lastInsertId());
}


//////-------------------

//         GET RAW DATA

//////-------------------
function getRawData($pdo,$data,$more=null)
{

				$db=$pdo['pdo'];
				$table=$pdo['table'];
				 
				$select_condition=isset($more['select_condition'])? $more['select_condition']:null;
				$where=isset($select_condition)? 'WHERE '.$select_condition.' ':'';
				$order=isset($pdo['order'])?$pdo['order']:null;
				$orderby=isset($order)?' ORDER BY '.$order:'';
				$select=implode(',',$data);
				
				$field_nbr=count($data);
				///// trim as from the name of the field
				for($d=0; $d<$field_nbr ;++$d)
				{
					$pos = strpos($data[$d], " as ");
					if($pos !== false)
					{
					$data[$d]=explode(' as ', $data[$d])[1];
					
					}
				
				}
				
				
				//// 
				
				
				$join='';//isset($pdo['join'])? ' inner join '.$pdo['join']['table'].' on '.$pdo['join']['on'] :'';
				
				
				if(isset($pdo['join']))
				{
					for($i=1,$c=count($pdo['join']);$i<=$c;++$i)
					$join.=' inner join '.$pdo['join']['join'.$i]['table'].' on '.$pdo['join']['join'.$i]['on'];
				}
				
				
				
				
				
				try
				{
					$query='SELECT '.$select.' FROM '.$table.''.$join.' '.$where.' '.$orderby.' ';
					//echo $query;
					$data_list=$db->query($query);//ORDER BY '.$order.'
					if(!$data_list)
					{	
						$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
						throw new Exception($err);
					}
				}	
				catch(Exception $e)
				{
					die('Error while getting the data: '.$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a><br>');
				}

				
			return($data_list);			


}

//////-------------------

//         GET RAW DATA

//////-------------------
function CountRows($pdo,$more=null)
{

				$db=$pdo['pdo'];
				$table=$pdo['table'];
				 
				$select_condition=isset($more['select_condition'])? $more['select_condition']:null;
				$where=isset($select_condition)? 'WHERE '.$select_condition.' ':'';
				$order=isset($pdo['order'])?$pdo['order']:null;
				$orderby=isset($order)?' ORDER BY '.$order:'';
				 
				
				
				
				
				//// 
				
				
				$join='';//isset($pdo['join'])? ' inner join '.$pdo['join']['table'].' on '.$pdo['join']['on'] :'';
				
				
				if(isset($pdo['join']))
				{
					for($i=1,$c=count($pdo['join']);$i<=$c;++$i)
					$join.=' inner join '.$pdo['join']['join'.$i]['table'].' on '.$pdo['join']['join'.$i]['on'];
				}
				
				
				
				
				
				try
				{
					$query='SELECT COUNT(*) FROM '.$table.''.$join.' '.$where.' '.$orderby.' ';
					//echo $query;
					$count=$db->query($query);//ORDER BY '.$order.'
					if(!$count)
					{	
						$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
						throw new Exception($err);
					}
				}	
				catch(Exception $e)
				{
					die('Error while getting the data: '.$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a><br>');
				}

				
			return($count);			


}




			//---------------- 

//					EDIT RECORD
			//---------------- 
			function	editRecord($db_info,$form_data,$more)
			{
			
				$pdo=$db_info['pdo'];
				$table=$db_info['table'];
				$field_class=(isset($more['field_class']))?$more['field_class']:'';	
				//$join=isset($db_info['join'])? ' inner join '.$db_info['join']['table'].' on '.$db_info['join']['on'] ;
				// multiple join
				if(isset($db_info['join']))
				{
					if(is_array($db_info['join']['table']))
					{
					$join='';
					for($i=0,$c=count($db_info['join']['table']) ; $i<$c ; ++$i)
					{
					$join.= ' inner join '.$db_info['join']['table'][$i].' on '.$db_info['join']['on'][$i];
					}
					}
					else $join= ' inner join '.$db_info['join']['table'].' on '.$db_info['join']['on'];
				
				}else $join='';
				//print_r($join);
				$id_table=$db_info['id_table'];
				$id_target=$db_info['id_target'];
				$areas=isset($more['textareas'])?  $more['textareas']:array(''); 
				//print_r($areas);
				try
				{
					 $query='SELECT * FROM '.$table.''.$join.' WHERE '.$table.'.'.$id_table.'='.$id_target.' ';
					 //echo $query;
					 $edit=$pdo->query('SELECT * FROM '.$table.''.$join.' WHERE '.$table.'.'.$id_table.'='.$id_target.' ');
					 if(!$edit)
					{
						$err='(code:'.$pdo->errorInfo()[1].', state:'.$pdo->errorInfo()[0].')';
						throw new Exception($err);
					
					}
				}
				catch(Exception $e)
				{
					die("Error while editing ".$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a> ');
				
				}	 
					 $record=$edit->fetch(PDO::FETCH_ASSOC);
					
					 
					 $labels=array_keys($form_data);
					 //print_r($labels);
					 
					 if(isset($more['form_class']) && !empty($more['form_class']))
						echo'<div class="'.$more['form_class'].'">';
					 elseif(isset($more['form_id']) && !empty($more['form_id']))
						echo'<div id="'.$more['form_id'].'">';
					 
					 echo '<form method="post" action="'.$more['form_action'].'" enctype="multipart/form-data">';
					 //$first_field=0; assumed to be id;
					 echo '<div class="'.$field_class.'"><label for="'.$form_data[$labels[0]].'">'.$labels[0].'</label><br><input disabled="disabled" type="text" value="'.$record[$form_data[$labels[0]]].'" /></div><br><br>';
					 //hidden variable-> id 
					 echo'<input type="hidden" name="'.$form_data[$labels[0]].'" id="'.$form_data[$labels[0]].'" value="'.$record[$form_data[$labels[0]]].'" />	';
					 for($i=1,$c=count($labels);$i< $c ; ++$i)
					 {	
						if(isset($more['option_select'][$form_data[$labels[$i]]]))
						{	$identifier=$form_data[$labels[$i]];
							
							$opt_table=$more['option_select'][$identifier]['table'];
							$variable=$more['option_select'][$identifier]['variable'];
							$type=(isset($more['option_select'][$form_data[$labels[$i]]]['type']))? $more['option_select'][$form_data[$labels[$i]]]['type']: null;
							$db_info=array("pdo"=>$pdo,"table"=>$opt_table);
							//$data_option=array();[]
							$data_option=$more['option_select'][$identifier]['fields'];
							$more_option=array("variable"=>$variable,"type"=>$type,'back_url'=>$more['back_url']);
							//id_'.$table.','.$field.',tr1_'.$table.'
							$variable=['variable'];
							echo '<div class="'.$field_class.'"><label for="'.$form_data[$labels[$i]].'">'.$labels[$i].'</label><br>';
							option_select($db_info,$data_option,$more_option);
							echo'</div><br><br>';
							
							
							//$form_data[$labels[$i]],$form_data[$labels[$i]],);strlen($record[$form_data[$labels[$i]]])<30
						}elseif(!array_key_exists($form_data[$labels[$i]],$areas))//key($record[])
						echo '<div class="'.$field_class.'"><label for="'.$form_data[$labels[$i]].'">'.$labels[$i].'</label><br><input type="text" name="'.$form_data[$labels[$i]].'" id="'.$form_data[$labels[$i]].'" value="'.$record[$form_data[$labels[$i]]].'" /></div><br><br>';
						elseif($areas[$form_data[$labels[$i]]]==='editor'){
							echo'<div class="'.$field_class.'"><label for="'.$form_data[$labels[$i]].'">'.$labels[$i].'</label><br><textarea name="'.$form_data[$labels[$i]].'" id="'.$form_data[$labels[$i]].'"/>'.$record[$form_data[$labels[$i]]].'</textarea></div><br><br>';
							}elseif($areas[$form_data[$labels[$i]]]==='noeditor')
							{
								echo'<div class="'.$field_class.'"><label for="'.$form_data[$labels[$i]].'">'.$labels[$i].'</label><br><textarea class="NoEditor"  name="'.$form_data[$labels[$i]].'" id="'.$form_data[$labels[$i]].'"/>'.$record[$form_data[$labels[$i]]].'</textarea></div><br><br>';
							}elseif(isset($areas[$form_data[$labels[$i]]]))
							{
								echo'<div class="'.$field_class.'"><label for="'.$form_data[$labels[$i]].'">'.$labels[$i].'</label><br><textarea class="NoEditor" maxlength="'.$areas[$form_data[$labels[$i]]].'" name="'.$form_data[$labels[$i]].'" id="'.$form_data[$labels[$i]].'"/>'.$record[$form_data[$labels[$i]]].'</textarea></div><br><br>';
							}
					 
					 }
					 
					 // test if image
					 if(isset($more['image']) && $more['image']!= false) 
					 echo'<br><img style="max-width:200px; max-height:200px;" src="'.$more['image'].'"/>
									<div class="info">
										<label for="image">IMAGE:</label>
										<br>
										<input type="file" name="image" id="image"  />
									</div><br><br>';
					 elseif(isset($more['image']) && $more['image']=== false)echo'<div class="info">
										<label for="image">IMAGE:</label>
										<br>
										<input type="file" name="image" id="image"  />
									</div><br><br>';


					// test if file
					if(isset($more['file']) && $more['file']['src']!= false) 
					 echo'<br> <a href="'.$more['file']['src'].'"> <img style="max-width:100px; max-height:100px;" src="'.$more['file']['icon'].'"/> </a>

									<div class="info">
										<label for="file">FILE:</label>
										<br>
										<input type="file" name="file" id="file"  />
									</div><br><br>';
					 elseif(isset($more['file']['src']) && $more['file']['src']=== false)echo'<div class="info">
										<label for="file">FILE:</label>
										<br>
										<input type="file" name="file" id="file"  />

									</div><br><br>';
					 
					 //hidden variables
					 echo'<input  type="hidden" name="update"  value="true"/>';
					 
					 echo '	<div class="'.$field_class.'">
								<input type="submit" value="SAVE" name="submit"/>
								<input type="reset"  value="CLEAR"/>
							</div></form>';
					if((isset($more['form_class']) && !empty($more['form_class'])) ||(isset($more['form_id']) && !empty($more['form_id'])) )
						echo'</div>';
					 
							 
				 
				
				
			
			}
			//---------------- 
//                             UPDATE RECORD
			//---------------- 
			function updateRecord($pdo,$data,$more)
			{
				$db=$pdo['pdo'];
				$table=$pdo['table'];
				unset($data['update']);
				unset($data['submit']);
				$id=reset($data); // get the value of the primary key , which is the first key of Data
				$key_id=key($data);// Get the name of the primary key , which is the first key of Data
				unset($data[$key_id]); // remove the ID (primary Key)
				
				//trim($_POST['title'])==null&& trim($data[$data_index[0]])==' '
				//if(empty($data[$data_index[0]]) )die('Some fields are missing.<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a>');

				$data_index=array_keys($data);
				//print_r($data_index);
				$set=$data_index[0].'= ? '; 
				$update_array=[$data[$data_index[0]]];
				for($i=1, $c=count($data); $i<$c ;++$i)
				{	//if(empty($data[$data_index[$i]]))die('Some fields are missing.<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a>');
					$set.=','.$data_index[$i].'= ? ' ;//\''.$data[$data_index[$i]].'\'
					$update_array[$i]=$data[$data_index[$i]];
				}
				//echo $set.' '.$key_id.'='.$id;
				
				
				try
				{
					$q='UPDATE '.$table.' SET '.$set.' WHERE '.$key_id.'=\''.$id.'\' ';
				    $update=$db->prepare($q);//?
					//print_r($q);
					//print_r($update_array);
					$update->execute($update_array);//array($id)
					if(!$update)
					{
						$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
						throw new Exception($err);
					
					}
				
				
				}
				catch( Exception $e)
				{
					die("Error while updating ".$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a> ');
				
				}
				if(!isset($more['update_nbr'])||$more['update_nbr']===1)die('Record updated successfully.<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a>');
			
			}
			//---------------- 
//                                  UPDATE MULTIPLE RECORD
			//---------------- 
			function update_multiple($pdo,$data,$more)
			{
				
				$db=$pdo['pdo'];
				$table=$pdo['table'];
				$keys=array_keys($data);
				//$values=implode(',',$keys);
				
				/* $id=reset($keys);
				$key_id=key($keys);
				$unknown='?';
				for($i=1,$c=count($keys);$i<$c;++$i)
				$unknown.=',?'; */
				
				$getID_tr=$data['id_match'];
				try
				{	$result=$db->query('SELECT '.$getID_tr.' FROM '.$table.' WHERE '.$keys[1].'='.$data[$keys[1]].' ');

					if(!$result)
					{
						$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
						throw new Exception($err);
					
					}
									
				}
				catch(Exception $e)
				{
					die('Error while updating '.$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a> ');	
				}
				$id_tab=[];
				$i=0;
				while($record=$result->fetch())
				{
					$id_tab[$i]=$record[$getID_tr];
					++$i;
				}
				//print_r($id_tab);
				for($i=0,$c=count($id_tab);$i<$c;++$i)
				{
					try
					{	$result=$db->query('delete from '.$table.' WHERE '.$getID_tr.'='.$id_tab[$i].'');
						if(!$result)
						{
							$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
							throw new Exception($err);
						
						}
										
					}
					catch(Exception $e)
					{

						die('Error while updating '.$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a> ');	

					}
				}					
				//$link_Method=$db->query('UPDATE '.$table.' SET '.$set.' WHERE '.$key_id.'=\''.$id.'\' ');
			
			/* insert into Product-method */
			reset($data[$keys[2]]);
			$first=key($data[$keys[2]]);
			end($data[$keys[2]]);
			$last=key($data[$keys[2]]);
			$j=0; //to go through $id_tab
			for($i=$first;$i<=$last ;++$i )
			{
				if(isset($data[$keys[2]][$i]))
				{
				  //$link_Method->execute(array($theID,$i)); 
				  $fields=$keys[1].','.$keys[2];//$keys[0].','. //.$data[$keys[0]].','
				  //$db->query('UPDATE '.$table.' SET '.$keys[2].'='.$i.' WHERE '.$getID_tr.'=\''.$id_tab[$j].'\' ');
				  
				  try
				  {	$result=$db->prepare('INSERT INTO '.$table.' ('.$fields.') VALUES(\''.$data[$keys[1]].'\',\''.$i.'\') ');
					$result->execute();
					if(!$result)
					{	
						$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
						throw new Exception($err);
					}
				  }
				  catch(Exception $e)
				  {
					die('Error while updating '.$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a>');
				  }
				  
				  
				  
				  //echo $error;$error=
				  //echo'INSERT INTO '.$table.' ('.$fields.') VALUES(\''.$data[$keys[1]].'\',\''.$i.'\')';
				  //$j++;
				}
			}
			
			}
			//---------------- 
//				DELETE RECORD
			//---------------- 
			function	deleteRecord($pdo,$table,$id_table,$id,$back_url)
			{
				try
				{
				 $del=$pdo->query('DELETE FROM '.$table.' WHERE '.$id_table.'='.$id.' ');
					//$del->execute();errorCode()
					
					if(!$del)
					{	
						$err='(code:'.$pdo->errorInfo()[1].', state:'.$pdo->errorInfo()[0].')';
						throw new Exception($err);
					}
				}
				catch(Exception $e)
				{
					die("Error while deleting.  ".$e->getMessage().'<br><br><a class="option_button" href="'.$back_url.'">Return</a>');
					
				}
				//die('Record deleted successfully.<br><br><a class="option_button" href="'.$back_url.'">Return</a>');

			}
			//---------------- 
// 						LIST DATA FROM DATA BASE
			//---------------- 
			function listData($pdo,$data,$more)
			{
				$db=$pdo['pdo'];
				$table=$pdo['table'];
				$display_id=isset($pdo['display_id'])? $pdo['display_id']:'OFF';
				/* reset($data);
				$id_key=key($data); */
				$select_condition=isset($more['select_condition'])? $more['select_condition']:null;
				$where=isset($select_condition)? 'WHERE '.$select_condition.' ':'';
				$order=isset($pdo['order'])?$pdo['order']:null;
				$orderby=isset($order)?' ORDER BY '.$order:'';
				$select=implode(',',$data);
				$limit=isset($more['limit'])?' LIMIT '.$more['limit']:'';
				$field_nbr=count($data);
				/////test
				for($d=0; $d<$field_nbr ;++$d)
				{
					$pos = strpos($data[$d], " as ");
					if($pos !== false)
					{
					$data[$d]=explode(' as ', $data[$d])[1];
					
					}
				
				}
				
				
				////test
				
				$url_inspect=isset($more['inspect_url'])? $more['inspect_url']:null;
				$url_edit=isset($more['edit_url'])? $more['edit_url']:null;
				$url_delete=isset($more['delete_url'])? $more['delete_url']:null;
				$titles=$more['titles'];
				$join='';//isset($pdo['join'])? ' inner join '.$pdo['join']['table'].' on '.$pdo['join']['on'] :'';
				$position=null;
				$image=isset($more['image'])? $more['image']:null;
				$backItem=isset($more['back_item'])?'&amp;'.$more['back_item']:'';
				
				if(isset($pdo['join']))
				{
					for($i=1,$c=count($pdo['join']);$i<=$c;++$i)
					$join.=' inner join '.$pdo['join']['join'.$i]['table'].' on '.$pdo['join']['join'.$i]['on'];
				}
				
				
				if(isset($more['multi_data']))
				{
					$multi_table=$more['multi_data']['table'];
					$select_tab=$more['multi_data']['select'];
					$select_nbr=count($select_tab);
					$multi_select=implode(',',$select_tab);
					$position=$more['multi_data']['position'];
					$condition=$more['multi_data']['condition'];
					$multi_join='';
					for($i=1,$c=count($more['multi_data']['join']);$i<=$c;++$i)
					$multi_join.=' inner join '.$more['multi_data']['join']['join'.$i]['table'].' on '.$more['multi_data']['join']['join'.$i]['on'];
					
					
				}
				try
				{
					$query='SELECT '.$select.' FROM '.$table.''.$join.' '.$where.' '.$orderby.' '.$limit.' ';
					//echo $query;
					$data_list=$db->query($query);//ORDER BY '.$order.'
					 
					$countRow=	$data_list->rowCount();
					if(!$data_list)
					{	
						$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
						throw new Exception($err);
					}
					 
				}	
				catch(Exception $e)
				{
					die('Error while getting the data: '.$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a><br>');
				}	
					
					$diff=0;
					echo'<table><tr class="table_title">';
					foreach($titles as $title)echo'<th>'.$title.'</th>';
					echo'</tr>';
					
					while($record=$data_list->fetch())
					{$multi_pos=1;
						//echo'<tr><td>'.$record['id_provider'].'</td><td>'.$record['nm_provider'].'</td><td><a href="new_provider.php?edit='.$record['id_provider'].'">EDIT</a> <a href="new_provider.php?del='.$record['id_provider'].'">DELETE</a></td></tr>';
						if(isset($more['multi_data']))
						{
							
							try
							{
								$multi_data=$db->query('SELECT '.$multi_select.' FROM '.$multi_table.''.$multi_join.' WHERE '.$condition.'='.$record[0].' ');
								if(!$multi_data)
								{	
									$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
									throw new Exception($err);
								}
							}
							catch(Exception $e)
							{
								die('Error while getting the data: '.$e->getMessage().'<br><br><a class="option_button" href="'.$more['back_url'].'">Return</a><br>');
							}
							
							$hash='';
							
							
							while($fetch=$multi_data->fetch())
							{
								if($select_nbr>1)$hash.=implode(',',$fetch).'.<br>';
								else $hash.=$fetch[$select_tab[0]].'.<br>';
							
							}
							
						}
						
						if($diff%2===0)echo'<tr class="line_type_one">';
						else echo'<tr class="line_type_two">';
						++$diff;
						$i=($display_id=='on')? 0:1 ;
						for($i; $i<$field_nbr;++$i)
						{
							if($multi_pos===$position && isset($more['multi_data'])) echo'<td>'.$hash.'</td>';
							if($image===$data[$i])echo'<td><img style="max-width:200px; max-height:150px;" src='.$record[$data[$i]].' /></td>';
							else echo'<td>'.$record[$data[$i]].'</td>';
							++$multi_pos;
						}
						if(isset($url_inspect) ||isset($url_edit) ||isset($url_delete))
						{
							echo'<td class="actions">';
							if(isset($url_inspect))echo' <a class="option_button" href="'.$url_inspect.'='.$record[0].'">INSPECT</a> ';
							if(isset($url_edit))echo' <a class="option_button" href="'.$url_edit.'='.$record[0].$backItem.'">EDIT</a> ';
							if(isset($url_delete))echo' <a class="option_button" href="'.$url_delete.'='.$record[0].$backItem.'">DELETE</a> ';
							echo'</td>';
						}
						echo'</tr>';
					}
					echo'</table>';
				
			return($countRow);
			}	
			//-----------------------------
//				LOAD DATA IN HTML SELECT TAG            --- need at least two fields id + ...
			//---------------------------
			function option_select($pdo,$data,$more)
			{//$table,$field,$variable,$db,$type=null,$trans=null
				
				$db=$pdo['pdo']; // was pdo in older version !
				$table=$pdo['table'];
				$back_url=isset($more['back_url'])?$more['back_url']:'';
				$select=implode(',',$data);//id_'.$table.','.$field.',tr1_'.$table.'
				$variable=$more['variable'];// echo $select;
				//$id= for future work: need to make the id dynamicly assigned
				$class=	isset($more['class'])? 'class='.$more['class']:'';
				$data_variables= isset($more['data'])? ''.$more['data']:'';
				$selected=isset($more['selected'])?$more['selected']:''; 
				$condition=isset($more['condition'])?$more['condition']:''; 
			  	
				try
				{ $query=$db->query('SELECT '.$select.' FROM '.$table.' '.$condition.' ');
					//$query->execute();
					if(!$query)
							{	
								$err='(code:'.$db->errorInfo()[1].', message:'.$db->errorInfo()[2].')';
								throw new Exception($err);
							}
				}
				catch(Exception $e)
				{
					die('Error while getting the data'.$e->getMessage().'<br><br><a class="option_button" href="'.$back_url.'">Return</a>');
				
				}
				if(!isset($more['type']))
				{
					
					
					echo'<select  '.$class.' '.$data_variables.' name="'.$variable.'" id="'.$variable.'" >
					<option></option>';
					while($fetch=$query->fetch())
					{	$value=$fetch[1];
						if(isset($selected['type']) && $selected['type']=='id') $match=$fetch[0]; 
						 else $match=$fetch[1];
						for($i=2,$c=count($data);$i < $c;++$i)$value.=' -- '.$fetch[$i];
						if(isset($selected['value']) && $match===$selected['value'] )
						{
						if(isset($more['optionval']) && $more['optionval']==='value') echo'<option selected value="'.$fetch[1].'">'.$value.'</option><br>';
						// "value" means that the returned value is the value itself and not a key. (ex: return the name of the thing instead of its id ) 
						else echo'<option selected value="'.$fetch[0].'">'.$value.'</option><br>'; 
						}elseif(isset($more['optionval']) && $more['optionval']==='value') echo'<option  value="'.$fetch[1].'">'.$value.'</option><br>';
						else echo'<option  value="'.$fetch[0].'">'.$value.'</option><br>';
						
					}
					echo'</select>';
				}elseif($more['type']==='checkbox')  // selected option doesn't apply yet
					{
					
					
					$tab= [];
					//$i=1;// correspond to the first ID of method table
					while($fetch=$query->fetch())
					{	
						$value=$fetch[1];
						for($i=2,$c=count($data);$i < $c;++$i)$value.=' -- '.$fetch[$i];
						
						$tab[$fetch[0]]=$fetch[0];  ///  (corrected)BUG !!! BUG!!!   ids are not necessarily between 0 and 4  need to be fixed!!
						echo'<input type="checkbox" name="$tab['.$fetch[0].']" id="$tab['.$fetch[0].']" /> <label class="checkbox" for="$tab['.$fetch[0].']">'.$value.'</label><br />';
						//++$i;
					}
					
					
					
					}
			  
			}


///------------------------------------------------------------------------------------------------------------------
/// Random salect         !warning! when "RANDOM" is ACTIVE, this function should be used only in a database with less than 1000 rows ! 
///                      last update: it takes an id as a filename of an image and a name as a title, and displays items 
///------------------------------------------------------------------------------------------------------------------
function item_select($pdo,$data,$options)
{

	$db=$pdo['db'];
	$table=$pdo['table'];
	$fields=$data['fields'];
	$select_fields=implode(',',$fields);
	$img_info=$data['img'];	
	$path=$img_info['path'];
	$name=$img_info['name'];
	$title=$data['title'];
	$number_of_rows=$options['rows'];
	$where=	$options['where'];
	$href=	$options['href_info']['href'];	
	$var_href=$options['href_info']['var'];
	$rand=( isset($option['rand']) &&  $option['rand']==='true' )? "ORDER BY RAND()":"" ;	
	$query="SELECT ".$select_fields." FROM ".$table." WHERE ".$where." ".$rand." LIMIT ".$number_of_rows."";
	//echo $query;	
	$random=$db->query($query);
	while($record=$random->fetch(PDO::FETCH_ASSOC))
	{
		$src=isset(glob($path.$record[$name].'.*')[0]) ? glob($path.$record[$name].'.*')[0]:'images/icons/not_available.png';		
		echo"<div><a href='".$href."?".$var_href."=$record[$name]' ><p><img src='".$src."' alt='picture of product'/></a></p><h5>".$record[$title]."</h5></div>";

	}

}

/////////////-------------------------------------------------------------------------------------------------------------------------
////////////                                  Add/edit image
////////////--------------------------------------------------------------------------------------------------------------------------
function ae_image($file,$lastID,$gopath)
{

				



	$extensions=array('jpg','jpeg','png','gif');
	$infoFile=pathinfo($file['name']);
	$ext=$infoFile['extension'];
	
	$src=isset(glob($gopath.$lastID.'.*')[0]) ? glob('../images/products/'.$_POST['id_product'].'.*')[0]:'';
	//echo $src;
	if($src!='') unlink($src);

	if(in_array($ext,$extensions))
	{	
	/*validate picture*/
	$path=$gopath.$lastID.".".$ext;
	move_uploaded_file($file['tmp_name'],$path);

	
	
	}else echo "Extension not recognized.";
			



}
//// testing validate date
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

?>			
